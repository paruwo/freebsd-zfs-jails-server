# Small home server using FreeBSD + zfs + jails

Here are some snippets to set up my small home server, just for fun.

### Why?

Of course, cloud is fun. But if you still own some (older) computer and enjoy some
hacking at home, a local FreeBSD server with jails in place is a polished system
with lots of capabilities as well as another source to learn new stuff.


### What?

So basically, there is a groundwork setup (setup) plus two possible jail paths,
one for plain vanilla jails (setup/jails) and one for iocage (setup/iocage).


### Assumptions

 * recreational and fun project
 * pragmatic and straight forward
 * security is no concern (don't use anything of that in real-world internet-facing environments)
 * automation is not worth for me yet
 * ok to build own ports (benefit from changed options and play with optimizations)


# Resources

More resources can be found in the respective jails files.
For any real-world applications, do check out the real documentation
as well as Michael Warren Lucas' excellent books on zfs and jails.

reserved IPs for this setup:
 * postgresql .2 (legacy)
 * postgresql14 .214
 * jenkins .3, .4, .17
 * redis .6
 * airflow .7
 * influxd .10
 * grafana .11
 * kafka .12
 * poudriere .13
 * jupyter .14
 * weather .16
 * containerd .18
 * buildbot .20, .21




## TODO

 * replace manual stuff by executable scripts
 * Besides 1) plain jails and 2) iocage and 3) bhyve try out CBSD as clustered setup


## Known issues

 * plain jails: building ruby as dependency for git does not work, where LOIP4 settings are corrupt; only works
   with awkward manual workarounds not mentioned here
 * gocd app is not working (several issues like remote agents cannot not be found)
 