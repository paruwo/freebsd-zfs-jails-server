### install from medium
# separate SWAP partition with 24+ GB


### first update and adjustments
mkdir -p /usr/local/etc/pkg/repos
cat <<EOT >> /usr/local/etc/pkg/repos/FreeBSD.conf 
FreeBSD: {
  url: "pkg+http://pkg.FreeBSD.org/${ABI}/latest"
}
EOT


freebsd-update fetch install
pkg -4 update && pkg -4 upgrade

# basic networking tools like dig, nslookup etc.
pkg -4 install bind-tools


# local_unbound
# see, https://www.privacy-handbuch.de/handbuch_93d.htm
# and https://revprez.github.io/posts/2017-09-09-local_unbound-on-freebsd-11.html
#
# 192.168.178.0 is my local intranet, where 192.168.178.1 is my fritzbox
# sysrc local_unbound_enable="YES"
# local-unbound-setup 192.168.178.1 146.185.167.43 80.241.218.68 46.182.19.48
# service local_unbound restart
# ping www.google.de



# check and install file etc_rc.conf
# check and install file etc_pf.conf
# check and install file usr_loca_etl_doas.conf
# check and install file etc_sysctl.conf
# check and install file etc_fstab
# mount for now:
mount -t fdescfs null /dev/fd
mount -t devfs devfs /dev



# adjust power consumption for now:
sysrc powerd_enable="YES"
sysrc powerd_flags="-n adaptive -a adaptive"

# https://wiki.freebsd.org/TuningPowerConsumption
# echo 'hint.atrtc.0.clock=0' >> /boot/loader.conf
# echo 'amdtemp_load="YES"' >> /boot/loader.conf
# kldload amdtemp  # for now

sysctl dev.cpu.0.cx_supported
# (yes, "C2" is deepest sleep state, but so it's reusable)
sysrc performance_cx_lowest="Cmax"

# set deepest available sleep for now (given CPU AMD FX-8350)
# This seems not to got stored, but we will not mess around with further system timers,
# cf. https://forums.freebsd.org/threads/c-states-not-used.66192/page-2.

# Manually set lowest c-state for now:
sysctl dev.cpu.0.cx_lowest=C2
sysctl dev.cpu.1.cx_lowest=C2
sysctl dev.cpu.2.cx_lowest=C2
sysctl dev.cpu.3.cx_lowest=C2
sysctl dev.cpu.4.cx_lowest=C2
sysctl dev.cpu.5.cx_lowest=C2
sysctl dev.cpu.6.cx_lowest=C2
sysctl dev.cpu.7.cx_lowest=C2



# find fastest pkg mirror, if needed
# pkg -4 install py38-fastest_pkg
# fastest_pkg


# install most standard tools
pkg -4 install tmux vim-tiny curl doas

# add to ~/.login_conf (as root and user)
me:\
        :charset=UTF-8:\
        :lang=de_DE.UTF-8:\
        :setenv=LC_COLLATE=C:


# allow and mount devfs for now only
sysctl security.jail.mount_devfs_allowed=1



# optionally, only if not done on initial setup
# add 24GB swap for overall 32GB
dd if=/dev/zero of=/usr/swap0 bs=1G count=24
chmod 0600 /usr/swap0
echo 'md99 none swap sw,file=/usr/swap0,late 0	0' >> /etc/fstab
# set for running system if needed: swapon -aq, swapinfo -m

# activate right now and check
swapon -aq
swapinfo -kh



# an APC battery present and connected?
# pkg install apcupsd
# sysrc apcupsd_enable="YES"
# vim /usr/local/etc/apcupsd.conf



# collectd5
pkg -4 install collectd5

# set FQDNLookup=false, set hostname
# enable: df, disk, processes, swap, uptime, zfs_arc
vim /usr/local/etc/collectd.conf
# network: 192.168.0.10

service collectd enable
service collectd start
# for errors check logs in
# tail /var/log/messages



# yes, it can use itself as source
cat <<EOT >> /usr/local/etc/pkg/repos/poudriere.conf
poudriere: {
    url             : "http://192.168.0.13:8080/packages/130amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 100
}
EOT

# .. and remove official repo - but only if poudriere was was setup and works
cat <<EOT >> /usr/local/etc/pkg/repos/FreeBSD.conf
FreeBSD: {
    enabled: no
}
EOT


# when poudriere is configured but still is not considered, try setting "enabled: no" and update pkg
# vim /etc/pkg/FreeBSD.conf
# pkg -4 update
# pkg -4 upgrade



# manage network traffic through pf
sysrc cloned_interfaces="lo1"
# local network ranges for jails, this one creates enough local endpoints
sysrc ipv4_addrs_lo1="192.168.0.1-30/27"
service netif restart lo1
# check with "ifconfig lo1" that 30 new inet alias entries occur


# install /etc/pf.conf and go on
service netif restart
service pf enable
service pf start


# it's a good idea to reboot once before continuing to install jails
# this ensures that all services are up and running as expected
