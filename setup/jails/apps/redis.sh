
### 1) ZFS
zfs create -o mountpoint=/usr/local/jails/redis -o quota=10G jails/redis
tar -xpf /usr/local/jails/media/13.1/base.txz -C /usr/local/jails/redis



### 2) put to /etc/jail.conf
cat <<EOT >> /etc/jail.conf

redis {
  ip4.addr="192.168.0.6";
};
EOT

# only if poudriere active, then do modify repos:
mkdir -p /usr/local/jails/redis/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/redis/usr/local/etc/pkg/repos/poudriere.conf
poudriere: {
    url             : "http://192.168.0.13:8080/packages/130amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

# in doubt set "enabled: no"
# vim /usr/local/jails/redis/etc/pkg/FreeBSD.conf



### 3) basic jail setup
cp /etc/resolv.conf /usr/local/jails/redis/etc/
cp /etc/localtime /usr/local/jails/redis/etc/
touch /usr/local/jails/redis/etc/fstab

service jail start redis

sysrc -j redis sendmail_enable="NO"
sysrc -j redis sendmail_submit_enable="NO"
sysrc -j redis sendmail_outbound_enable="NO"
sysrc -j redis sendmail_msp_queue_enable="NO"

cpuset -j redis -cl 5
rctl jail:redis:vmemoryuse:deny=1G

echo 'rdr on $ext_if proto tcp from any to any port 6379 -> 192.168.0.6 port 6379' >> /etc/pf.conf
service pf restart


cat <<EOT >> /etc/hosts
192.168.0.6 redis redis.fritz.box
EOT


# optional collectd (take own build with postgres enabled)
pkg -4 -j redis install collectd5

# set FQDNLookup=false, set hostname
# activate: redis
vim /usr/local/jails/redis/usr/local/etc/collectd.conf
# network: 192.168.0.10
# enable: redis + config

service -j redis collectd enable
service -j redis collectd start
# for errors check logs in
# tail /usr/local/jails/redis/var/log/messages




### 4) app setup
pkg -4 -j redis install redis vim-tiny
service -j redis redis enable
service -j redis redis start

# test it / tiny benchmark
jexec -l redis redis-benchmark -h localhost -t set -n 10000000 -d 4 -c 8 -q -P 16
