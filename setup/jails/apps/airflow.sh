### 1) ZFS
# zfs destroy jails/airflow
zfs create -o mountpoint=/usr/local/jails/airflow -o quota=10G jails/airflow
tar -xpf /usr/local/jails/media/13.1/base.txz -C /usr/local/jails/airflow


### 2) put to /etc/jail.conf
cat <<EOT >> /etc/jail.conf

airflow {
  ip4.addr="192.168.0.7";
  depend="postgresql";
  depend="redis";
};
EOT

# example for one jail (same for other jails)
mkdir -p /usr/local/jails/airflow/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/airflow/usr/local/etc/pkg/repos/poudriere.conf
poudriere: {
    url             : "http://192.168.0.13:8080/packages/130amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

# in doubt set "enabled: no"
# vim /usr/local/jails/airflow/etc/pkg/FreeBSD.conf

cat <<EOT >> /etc/hosts
192.168.0.7 airflow airflow.fritz.box
EOT






### 3) basic jail setup
cp /etc/resolv.conf /usr/local/jails/airflow/etc/
cp /etc/localtime /usr/local/jails/airflow/etc/
touch /usr/local/jails/airflow/etc/fstab

service jail start airflow

sysrc -j airflow sendmail_enable="NO"
sysrc -j airflow sendmail_submit_enable="NO"
sysrc -j airflow sendmail_outbound_enable="NO"
sysrc -j airflow sendmail_msp_queue_enable="NO"

cpuset -j airflow -cl 6,7
rctl jail:airflow:vmemoryuse:deny=2G

echo 'rdr on $ext_if proto tcp from any to any port 8087 -> 192.168.0.7 port 8080' >> /etc/pf.conf
echo 'rdr on $ext_if proto tcp from any to any port 5555 -> 192.168.0.7 port 5555' >> /etc/pf.conf
service pf restart


### 4) app setup
# install packages
pkg -4 -j airflow install python3 git-tiny tmux vim-tiny bash postgresql13-contrib rust py38-sqlite3 py38-virtualenv

# setup app user
jexec -l airflow pw user add airflow -m

# install airflow via python
jexec -l airflow login -f airflow
python3.8 -m ensurepip --user
python3.8 -m pip install --upgrade pip setuptools --user
python3.8 -m pip install psycopg2-binary --user
SLUGIFY_USES_TEXT_UNIDECODE=yes python3.8 -m pip install apache-airflow>=2.2 --user
SLUGIFY_USES_TEXT_UNIDECODE=yes python3.8 -m pip install apache-airflow[crypto,redis,celery,async,postgresql] --user

# Assumption is that jails redis + postgresql are up & running
# this is $AIRFLOW_HOME
mkdir -p /home/airflow/airflow
cd /home/airflow/airflow
vim airflow.cfg

# airflow.cfg
[core]
default_timezone = Europe/Berlin
sql_alchemy_conn = postgresql+psycopg2://airflow:airflow@192.168.0.2:5432/airflow
executor = CeleryExecutor
load_examples = False

[webserver]
base_url = http://localhost:8080
web_server_host = 192.168.0.7
default_ui_timezone = Europe/Berlin

[scheduler]
dag_dir_list_interval = 30

[celery]
broker_url = redis://192.168.0.6:6379/0
result_backend = db+postgres://airflow:airflow@192.168.0.2:5432/airflow


# one-time initialization
.local/bin/airflow db init
echo "export PATH=$PATH:~/.local/bin" >> ~/.profile
# vim airflow/airflow.cfg



# in doubt check jail with
# doas jexec -l airflow login -f airflow
# nohup /usr/home/airflow/startup.sh &

# configure start/stop scripts
# @host
cat <<EOF >> /usr/local/jails/airflow/home/airflow/startup.sh
#!/bin/sh
nohup .local/bin/airflow celery worker --daemon &
nohup .local/bin/airflow scheduler --daemon &
nohup .local/bin/airflow webserver --daemon &
EOF

cat <<EOF >> /usr/local/jails/airflow/home/airflow/stop.sh
#!/bin/sh
kill $(ps aux | grep 'airflow webserver' | awk '{print $2}')
EOF

jexec -l airflow chown airflow:airflow /home/airflow/startup.sh
jexec -l airflow chown airflow:airflow /home/airflow/stop.sh
# jexec -l airflow chmod u+rwx /home/airflow/startup.sh
# jexec -l airflow chmod a+rwx /home/airflow/stop.sh

# create initial user
jexec -l -U airflow airflow .local/bin/airflow users create --role Admin --username admin --email admin \
  --firstname admin --lastname admin --password admin


# dependencies, add postgresql database user
jexec -l postgresql login -f postgres
psql
CREATE USER airflow WITH PASSWORD 'airflow' CREATEDB;
CREATE DATABASE airflow OWNER airflow;
\q
exit



# startup
jexec -l -U airflow airflow /home/airflow/startup.sh



# error handling

# debug
# go through all commands in startup.sh separately and call them as airflow user
# there should be no error
jexec -l airflow login -f airflow
.local/bin/airflow celery worker
# ...


# clean up any mess by recreate the database
jexec -l airflow login -f airflow
.local/bin/airflow db reset
.local/bin/airflow users create --role Admin --username admin --email admin --firstname admin --lastname admin --password admin
