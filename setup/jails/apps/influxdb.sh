### 1) ZFS
# zfs destroy jails/influxdb
zfs create -o mountpoint=/usr/local/jails/influxdb -o quota=50G jails/influxdb
tar -xpf /usr/local/jails/media/13.1/base.txz -C /usr/local/jails/influxdb



### 2) put to /etc/jail.conf
cat <<EOT >> /etc/jail.conf

influxdb {
  ip4.addr="192.168.0.10";
};
EOT

# only if poudriere active, then do modify repos:
mkdir -p /usr/local/jails/influxdb/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/influxdb/usr/local/etc/pkg/repos/poudriere.conf
poudriere: {
    url             : "http://192.168.0.13:8080/packages/130amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

# in doubt set "enabled: no"
# vim /usr/local/jails/influxdb/etc/pkg/FreeBSD.conf


cat <<EOT >> /etc/hosts
192.168.0.10 influxdb influxdb.fritz.box
EOT





### 3) basic jail setup
cp /etc/resolv.conf /usr/local/jails/influxdb/etc/
cp /etc/localtime /usr/local/jails/influxdb/etc/
touch /usr/local/jails/influxdb/etc/fstab

service jail start influxdb

sysrc -j influxdb sendmail_enable="NO"
sysrc -j influxdb sendmail_submit_enable="NO"
sysrc -j influxdb sendmail_outbound_enable="NO"
sysrc -j influxdb sendmail_msp_queue_enable="NO"

cpuset -j influxdb -cl 6,7
rctl jail:influxdb:vmemoryuse:deny=2G

cat <<EOT >> /etc/pf.conf
### influxdb
## outgoing
rdr on $ext_if proto tcp from any to any port 8082 -> 192.168.0.10 port 3000
rdr on $ext_if proto tcp from any to any port 8086 -> 192.168.0.10 port 8086
## incoming
# collectd
rdr on $ext_if proto udp from any to any port 25826 -> 192.168.0.10 port 25826
# udp
rdr on $ext_if proto tcp from any to any port 8089 -> 192.168.0.10 port 8089
# weather
rdr on $ext_if proto tcp from any to any port 8090 -> 192.168.0.10 port 8090
EOT

service pf restart



### 4) app setup
pkg -4 -j influxdb install influxdb vim-tiny collectd5

jexec -l influxdb service influxd enable
jexec -l influxdb service influxd start
sysrc -j influxdb influxd_user="root"
sysrc -j influxdb influxd_group="wheel"

# set hostname, set FQDNLookup to false
vim /usr/local/jails/influxdb/usr/local/etc/collectd.conf

# start only once to get types.db
jexec -l influxdb service collectd onestart
jexec -l influxdb service collectd onestop

# [[collectdb]]
# typesdb = "/usr/local/share/collectd/types.db"



# generic setup and gonfig
jexec -l influxdb login -f root
influx -unsafeSsl
create user admin with password 'admin' with all privileges
create database weather
create database collectd with duration 104w
exit  # from influx shell


# ssl
jexec -l influxdb login -f root
openssl req -x509 -nodes -newkey rsa:2048 -keyout /etc/ssl/influxdb-selfsigned.key -out /etc/ssl/influxdb-selfsigned.crt -days 10000
chown influxd:influxd /etc/ssl/influxdb-selfsigned.*

vim /usr/local/etc/influxd.conf
# check [http] section
https-enabled=true
https-certificate = "/etc/ssl/influxdb-selfsigned.crt"
https-private-key = "/etc/ssl/influxdb-selfsigned.key"

service influxd restart

# test connectivity from inside jail
influx -ssl -unsafeSsl
# test connectivity from server
influx -ssl -unsafeSsl -host 192.168.0.10
