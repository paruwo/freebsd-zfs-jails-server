# This is highly dependent on your needs.
# So basic install applies for all situations, but the rest is up to you.

### 1) ZFS
# # zfs destroy jails/jenkins-agent
zfs create -o mountpoint=/usr/local/jails/jenkins-agent -o quota=20G jails/jenkins-agent
tar -xpf /usr/local/jails/media/13.1/base.txz -C /usr/local/jails/jenkins-agent



### 2) put to /etc/jail.conf
cat <<EOT >> /etc/jail.conf

jenkins-agent {
  ip4.addr="192.168.0.4";
};
EOT


# only if poudriere active, then do modify repos:
mkdir -p /usr/local/jails/jenkins-agent/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/jenkins-agent/usr/local/etc/pkg/repos/poudriere.conf
poudriere: {
    url             : "http://192.168.0.13:8080/packages/130amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

# in doubt set "enabled: no"
# vim /usr/local/jails/jenkins-agent/etc/pkg/FreeBSD.conf

cat <<EOT >> /etc/hosts
192.168.0.4 jenkins-agent jenkins-agent.fritz.box
EOT





### 3) basic jail setup
cp /etc/resolv.conf /usr/local/jails/jenkins-agent/etc/
cp /etc/localtime /usr/local/jails/jenkins-agent/etc/
touch /usr/local/jails/jenkins-agent/etc/fstab

service jail restart jenkins-agent

sysrc -j jenkins-agent sendmail_enable="NO"
sysrc -j jenkins-agent sendmail_submit_enable="NO"
sysrc -j jenkins-agent sendmail_outbound_enable="NO"
sysrc -j jenkins-agent sendmail_msp_queue_enable="NO"

# limit to what makes sense
zfs set quota=15G jails/jenkins-agent
cpuset -j jenkins-agent -cl 2,3
rctl jail:jenkins-agent:vmemoryuse:deny=4G

# no pf settings as this is an internal service



### 4) app setup
pkg -4 -j jenkins-agent install openjdk8 git
# and whatever else is needed, e.g. boost-libs cmake ninja cxxtest cppcheck doxygen lcov
# assume we go for ssh agents

service -j jenkins-agent sshd enable
service -j jenkins-agent sshd onestart

# create user ci/ci
jexec -l jenkins-agent login -f root
adduser ci  # empty password
exit

# add agent in Jenkins www frontend


### decommission
service jail stop jenkins-agent
zfs destroy jails/jenkins-agent
