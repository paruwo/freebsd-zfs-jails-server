# https://github.com/freebsd/poudriere/wiki/poudriere_in_jail
# https://www.digitalocean.com/community/tutorials/how-to-build-and-deploy-packages-for-your-freebsd-servers-using-buildbot-and-poudriere
# https://wiki.bsdforen.de/howto/poudriere_guide.txt.html (for nginx)
# https://wissen.der-beweis.de/freebsd/makeconf_headless.html (build options)
# https://forums.freebsd.org/threads/pkg-package-repository-using-ports-mgmt-poudriere-with-or-without-zfs.38859/#post-285430 (repo usage)
# https://www.mimar.rs/blog/host-your-own-services-with-freebsd-jails-part-3-poudriere

# ports location: /usr/local/jails/poudriere/work/ports/default
# all packages:   /usr/local/jails/poudriere/work/data/packages/140amd64-default/All/

# Known issues:
#  1) in case the installation of packages does not happen (even with correct repo priority setting
#     applied), then try to remove and re-install


# login
jexec -l poudriere login -f root

# all ports
ls -laht /usr/local/jails/poudriere/work/data/packages/140amd64-default/All/ | more

########################################################################################


### 1) ZFS
# base directory
zfs create -o mountpoint=/usr/local/jails/poudriere -o quota=100G -o compression=lz4 zroot/jails/poudriere
tar -xpf /usr/local/jails/media/14.0/base.txz -C /usr/local/jails/poudriere

# nested volume to get poudriere work using zfs
zfs create -o jailed=on zroot/jails/poudriere/work

mkdir /usr/local/jails/poudriere/distfiles





### 2) register jail in /etc/jail.conf
# nullfs needed for nested jail
# tmpfs for poudriere option to use tmpfs
cat <<EOT >> /etc/jail.conf

poudriere {
  ip4.addr ="192.168.0.13";
  # ip4.addr+="lo1|127.0.0.13";
  allow.mount;
  allow.mount.procfs;
  allow.mount.zfs;
  allow.mount.nullfs;
  allow.mount.tmpfs;  # ???
  allow.raw_sockets;
  allow.socket_af;
  allow.sysvipc;
  allow.chflags;
  enforce_statfs=1;
  children.max=16;
  persist;
};
EOT




### 3) basic jail setup
cp /etc/resolv.conf /usr/local/jails/poudriere/etc/
cp /etc/localtime /usr/local/jails/poudriere/etc/
touch /usr/local/jails/poudriere/etc/fstab

service jail start poudriere
zfs jail poudriere zroot/jails/poudriere/work

sysrc -j poudriere sendmail_enable="NO"
sysrc -j poudriere sendmail_submit_enable="NO"
sysrc -j poudriere sendmail_outbound_enable="NO"
sysrc -j poudriere sendmail_msp_queue_enable="NO"

# no cpuset
rctl jail:poudriere:vmemoryuse:deny=4G

# on demand, for www status page
cat <<EOT >> /etc/pf.conf
# poudriere (html status pages)
rdr on $ext_if proto tcp from any to any port 8100 -> 192.168.0.13 port 8080
EOT

service pf restart

cat <<EOT >> /etc/hosts
192.168.0.13 poudriere poudriere.fritz.box
EOT


# optional collectd (take own build with postgres enabled)
pkg -4 -j poudriere install collectd5

# set FQDNLookup=false, set hostname
vim /usr/local/jails/poudriere/usr/local/etc/collectd.conf
# network: 192.168.0.10
# enable: processes, swap

service -j poudriere collectd enable
service -j poudriere collectd start
# for errors check logs in
# tail /usr/local/jails/postgresql/var/log/messages






### 4) app setup
pkg -4 -j poudriere install vim-tiny poudriere git-tiny ccache

# set the following
vim /usr/local/jails/poudriere/usr/local/etc/poudriere.conf
# yes, "jails" is host's zfs pool
#ZPOOL=zroot
#NO_ZFS=no
#FREEBSD_HOST=https://ftp.de.freebsd.org  # try ftp://.. if it fail
#RESOLV_CONF=/etc/resolv.conf
#BASEFS=/work
#MAX_MEMORY=4
#DISTFILES_CACHE=/distfiles
#CHECK_CHANGED_OPTIONS=verbose
#CCACHE_DIR=/var/cache/ccache
#ALLOW_NETWORKING_PACKAGES="python311"
#PARALLEL_JOBS=4
#NOLINUX=yes
#ALLOW_MAKE_JOBS_PACKAGES="pkg ccache py* llvm* gcc* pkg rust cmake"
#PRIORITY_BOOST="llvm* rust openjdk*"
#LOIP6=no
#LOIP4=127.0.0.13




jexec -U root poudriere mkdir -p /var/cache/ccache

cat <<EOT >> /usr/local/jails/poudriere/var/cache/ccache/ccache.conf
max_size = 16GB
EOT


# default build options, taken from https://wissen.der-beweis.de/freebsd/makeconf_headless.html
# we assume packages targeting to headless system
cat <<EOT >> /usr/local/jails/poudriere/usr/local/etc/poudriere.d/make.conf
# optimizations
CPUTYPE?=native
OPTIONS_SET=OPTIMIZED_CFLAGS CPUFLAGS

# LLVM is a long-runner, do not build for all architectures
WITHOUT_LLVM_TARGET_ALL=

# globally disable these options
OPTIONS_UNSET+= DOCS NLS X11 EXAMPLES CUPS DEBUG FONTCONFIG TEST IPV6
EOT

# create nested build jail "140amd64"
jexec -U root poudriere poudriere jail -c -j 140amd64 -v 14.0-RELEASE
jexec -U root poudriere poudriere ports -c




### 5) www frontend
pkg -4 -j poudriere install nginx

vim /usr/local/jails/poudriere/usr/local/etc/nginx/nginx.conf
# add this as last section of main http block:
# # virtual hosting
# include /usr/local/etc/nginx/vhosts/*;

mkdir -p /usr/local/jails/poudriere/usr/local/etc/nginx/vhosts/
cat <<EOT >> /usr/local/jails/poudriere/usr/local/etc/nginx/vhosts/poudriere.conf
server {
  listen 8080 default;
  server_name poudriere.lan;
  root /usr/local/share/poudriere/html;

  location /data {
    alias /work/data/logs/bulk;
    autoindex on;
  }

  location /packages {
    root /work/data;
    autoindex on;
  }
}
EOT
chown nobody:nobody /usr/local/jails/poudriere/usr/local/etc/nginx/vhosts/poudriere.conf

jexec -U root poudriere service nginx enable
jexec -U root poudriere service nginx restart

vim /usr/local/jails/poudriere/usr/local/etc/nginx/mime.types
# modify
# text/plain                          txt log;"

# for testing, call in browser <server>:8100




### 6) configure host to use this repo

# on host and in jails:
# create /usr/local/etc/pkg/repos/
# register new repo and disable official repo
# remove /usr/local/etc/pkg.conf if remaining from jail "cache"

# example for one jail (same for other jails)
mkdir -p /usr/local/jails/poudriere/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/poudriere/usr/local/etc/pkg/repos/poudriere.conf
poudriere: {
    url             : "http://192.168.0.13:8080/packages/140amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

# in doubt set "enabled: no"
# vim /usr/local/jails/poudriere/etc/pkg/FreeBSD.conf



### 7) regular tasks
# maintain list of ports to build, for instance
cat <<EOT >> /usr/local/jails/poudriere/usr/local/etc/package-list.txt
# host-related tools
editors/vim
sysutils/tmux
ftp/curl
benchmarks/iperf
security/doas
databases/sqlite3
lang/tcl86
ftp/lftp
#sysutils/iocage

# jail-specific "tiny" flavours of common tools
editors/vim@tiny
devel/git@tiny

# postgresql
databases/postgresql16-server
databases/postgresql16-contrib

# jenkins
#java/openjdk22
#devel/jenkins-lts

# airflow
#lang/python311
#shells/bash
#databases/py-sqlite3
#devel/py-virtualenv
# this is a new dependency from cryptography module
#lang/rust

# kafka
#devel/zookeeper
#net/kafka
#net/kcat

# redis
#databases/redis

# poudriere
ports-mgmt/poudriere
devel/ccache
www/nginx

# grafana
www/grafana9
#net-mgmt/collectd5

# influxdb
databases/influxdb

# weather
lang/python311
databases/py311-sqlite3
textproc/libxml2
textproc/libxslt

# jupyter
#www/py-notebook

EOT


# We have some networking issues with loopback https://github.com/freebsd/poudriere/issues/657

# simple reconfigurations
#    choose +pgsql, +python, +redis, +zookeeper
jexec -U root poudriere poudriere options -c -n -j 140amd64 net-mgmt/collectd5
#    chose -rdoc
jexec -U root poudriere poudriere options -c -n -j 140amd64 lang/ruby27

# ruby27 does not build at all, TODO fix LOIP4 or go for VNET setup
# https://github.com/freebsd/poudriere/issues/657
# crazy workaround for now:
#   run ruby until it fails (ensure fetch was done)
#   then switch config
#      vim /usr/local/jails/poudriere/usr/local/etc/poudriere.conf - comment out LOIP4
#      vim /etc/jails.conf - ip4.addr="lo0|127.0.0.1"
#   restart jail build ruby only
#   undo change, restart jail and go on with dependent ports



#### regular work
# update build jail
jexec -U root poudriere poudriere jail -u -j 140amd64
# update ports and rebuild list items if needed
jexec -U root poudriere poudriere ports -u -v



# We are also free to run huge packages separately and consequentially, however PRIORITY_BOOST is set.
# ...on all 8 cores of AMD FX-8350, 16GB RAM, striped dual HDD disk setup
# ...where all further jails were stopped
# If we do not run these monsters subsequentially, in worst case we hit out of swap space issues.
# ---
# takes 55min (consumes 4GB swap additionally if executed sequentially)
jexec -U root poudriere poudriere bulk -j 140amd64 lang/gcc10
# takes 45min
jexec -U root poudriere poudriere bulk -j 140amd64 lang/rust
# take 35min
# jexec -U root poudriere poudriere bulk -j 140amd64 devel/llvm11
jexec -U root poudriere poudriere bulk -j 140amd64 java/openjdk21


# run for rest of list
jexec -U root poudriere poudriere bulk -j 140amd64 -f /usr/local/etc/package-list.txt
# upgrade pkg's in jails
cd /usr/local/jails && ./update_pkg.sh

# clean all explicitly built ports
# jexec -U root poudriere poudriere bulk -C -j 140amd64 -f /usr/local/etc/package-list.txt

# check cache efficiency
# env CCACHE_DIR=/var/cache/ccache ccache -s
jexec -U root poudriere env CCACHE_DIR=/var/cache/ccache ccache -s
