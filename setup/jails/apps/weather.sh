### 1) ZFS
zfs create -o mountpoint=/usr/local/jails/weather -o quota=5G jails/weather
tar -xpf /usr/local/jails/media/13.1/base.txz -C /usr/local/jails/weather




### 2) put to /etc/jail.conf
cat <<EOT >> /etc/jail.conf

weather {
  ip4.addr="192.168.0.16";
};
EOT

# only if poudriere active, then do modify repos:
mkdir -p /usr/local/jails/weather/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/weather/usr/local/etc/pkg/repos/poudriere.conf
poudriere: {
    url             : "http://192.168.0.13:8080/packages/130amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

# in doubt set "enabled: no"
# vim /usr/local/jails/weather/etc/pkg/FreeBSD.conf


### 3) basic jail setup
cp /etc/resolv.conf /usr/local/jails/weather/etc/
cp /etc/localtime /usr/local/jails/weather/etc/
touch /usr/local/jails/weather/etc/fstab

service jail start weather

sysrc -j weather sendmail_enable="NO"
sysrc -j weather sendmail_submit_enable="NO"
sysrc -j weather sendmail_outbound_enable="NO"
sysrc -j weather sendmail_msp_queue_enable="NO"

cpuset -j weather -cl 7
rctl jail:weather:vmemoryuse:deny=1G

cat <<EOT >> /etc/pf.conf
# weather
rdr on $ext_if proto tcp from any to any port 8016 -> 192.168.0.16 port 8080
EOT

service pf restart

cat <<EOT >> /etc/hosts
192.168.0.16 weather weather.fritz.box
EOT




### 4) app setup
pkg -4 -j weather install vim-tiny git-tiny python39 libxml2 libxslt postgresql13-client py39-sqlite3

jexec -l weather login -f root

git clone https://codeberg.org/paruwo/Weather.git
cd Weather

# influxdb: endpoint=https://192.168.0.10:8086
# postgresql: host=192.168.0.2
vim weather/config/weather.toml

python3.9 -m venv venv
source venv/bin/activate.csh
pip install .



# Postgres, if needed
jexec -l postgresql login -f postgres
psql
CREATE USER weather WITH PASSWORD 'weather' CREATEDB;
CREATE DATABASE weather OWNER weather;
\q
exit




# app usage
jexec -l weather login -f root
cd Weather
git pull
source venv/bin/activate.csh && python3.9 Weather.py -m http -t rain air wind solar --full

# put to crontab (add empty line)
# @hourly cd /root/Weather && source venv/bin/activate.csh && python3.9 Weather.py -m http -t rain air wind solar --delta

# login
# jexec -l weather login -f root
