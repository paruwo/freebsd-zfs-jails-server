# never used that really, just basic setup

# this needs fdescfs and procfs mounted

### 1) ZFS
# zfs destroy jails/kafka
zfs create -o mountpoint=/usr/local/jails/kafka -o quota=20G jails/kafka
tar -xpf /usr/local/jails/media/13./base.txz -C /usr/local/jails/kafka



### 2) put to /etc/jail.conf
cat <<EOT >> /etc/jail.conf

kafka {
  ip4.addr="192.168.0.12";
};
EOT

# only if poudriere active, then do modify repos:
mkdir -p /usr/local/jails/kafka/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/kafka/usr/local/etc/pkg/repos/poudriere.conf
poudriere: {
    url             : "http://192.168.0.13:8080/packages/130amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

# in doubt set "enabled: no"
# vim /usr/local/jails/kafka/etc/pkg/FreeBSD.conf

cat <<EOT >> /etc/hosts
192.168.0.12 kafka kafka.fritz.box
EOT






### 3) basic jail setup
cp /etc/resolv.conf /usr/local/jails/kafka/etc/
cp /etc/localtime /usr/local/jails/kafka/etc/
touch /usr/local/jails/kafka/etc/fstab


service jail start kafka

sysrc -j kafka sendmail_enable="NO"
sysrc -j kafka sendmail_submit_enable="NO"
sysrc -j kafka sendmail_outbound_enable="NO"
sysrc -j kafka sendmail_msp_queue_enable="NO"

cpuset -j kafka -cl 5
rctl jail:kafka:vmemoryuse:deny=1G

echo 'rdr on $ext_if proto tcp from any to any port 9092 -> 192.168.0.12 port 9092' >> /etc/pf.conf
service pf restart



# optional collectd (take own build with postgres enabled)
pkg -4 -j kafka install collectd5

# set FQDNLookup=false, set hostname, no kafka connector
# enable zookeeper and configure it keeping standard values
vim /usr/local/jails/kafka/usr/local/etc/collectd.conf
# network: 192.168.0.10

service -j kafka collectd enable
service -j kafka collectd start
# for errors check logs in
# tail /usr/local/jails/kafka/var/log/messages




### 4) app setup
pkg -4 -j kafka install zookeeper kafka kafkacat vim-tiny
jexec -l kafka service zookeeper enable
jexec -l kafka service kafka enable
jexec -l kafka service zookeeper start
jexec -l kafka service kafka start

# test it, TODO validate
# jexec -l kafka kafkacat -b localhost:9092 -t test-topic
