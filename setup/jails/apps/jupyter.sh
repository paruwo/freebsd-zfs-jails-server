# http://server.fritz.box:8888

### 1) ZFS
zfs create -o mountpoint=/usr/local/jails/jupyter -o quota=10G jails/jupyter
tar -xpf /usr/local/jails/media/13.1/base.txz -C /usr/local/jails/jupyter


### 2) put to /etc/jail.conf
cat <<EOT >> /etc/jail.conf

jupyter {
  ip4.addr="192.168.0.14";
};
EOT

# only if poudriere active, then do modify repos:
mkdir -p /usr/local/jails/jupyter/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/jupyter/usr/local/etc/pkg/repos/poudriere.conf
poudriere: {
    url             : "http://192.168.0.13:8080/packages/130amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

# in doubt set "enabled: no"
# vim /usr/local/jails/jupyter/etc/pkg/FreeBSD.conf



### 3) basic jail setup
cp /etc/resolv.conf /usr/local/jails/jupyter/etc/
cp /etc/localtime /usr/local/jails/jupyter/etc/
touch /usr/local/jails/jupyter/etc/fstab

service jail start jupyter

sysrc -j jupyter sendmail_enable="NO"
sysrc -j jupyter sendmail_submit_enable="NO"
sysrc -j jupyter sendmail_outbound_enable="NO"
sysrc -j jupyter sendmail_msp_queue_enable="NO"

# cpuset -j jupyter -cl 5
# rctl jupyter:redis:vmemoryuse:deny=2G

echo 'rdr on $ext_if proto tcp from any to any port 8888 -> 192.168.0.14 port 8888' >> /etc/pf.conf
service pf restart


cat <<EOT >> /etc/hosts
192.168.0.14 jupyter jupyter.fritz.box
EOT




### 4) app setup
# login
# jexec -l jupyter login -f root
# jexec -l jupyter login -f jupyter

# https://project.altservice.com/issues/923
pkg -4 -j jupyter install vim-tiny py38-pip py38-notebook

jexec -l jupyter login -f root
pw add user -n jupyter -m -s /bin/sh -c "Jupyter Notebook"
su - jupyter -c "jupyter notebook --generate-config"
su - jupyter -c "mkdir -p /home/jupyter/.jupyter/"
exit

cat <<EOT >> /usr/local/jails/jupyter/home/jupyter/.jupyter/jupyter_notebook_config.py
c.NotebookApp.ip ='*'
c.NotebookApp.open_browser = False
c.NotebookApp.notebook_dir  = '/tmp'
c.NotebookApp.port = 8888
EOT


mkdir /usr/local/jails/jupyter/var/run/jupyter
mkdir -p /usr/local/jails/jupyter/usr/local/etc/rc.d/

cat <<EOT >> /usr/local/jails/jupyter/usr/local/etc/rc.d/jupyter
#!/bin/sh

. /etc/rc.subr

name=jupyter
command=/usr/local/bin/jupyter-notebook
rcvar=jupyter_enable

load_rc_config $name

jupyter_enable="${jupyter_enable-"NO"}"
jupyter_user="${jupyter_user-"jupyter"}"
jupyter_pidfile="${jupyter_pidfile:-"/var/run/jupyter/jupyter.pid"}"

# /etc/rc.subr use $pidfile (not ${name}_pidfile)
pidfile="${jupyter_pidfile}"

start_cmd="su - ${jupyter_user} -c '${command}' &"
stop_cmd="${name}_stop"
status_cmd="${name}_status"
getval_cmd="${name}_getval"

jupyter_stop()
{
    jupyter_pid=$(pgrep -f "jupyter-notebook")

    echo "Stopping ${name}."
    kill -s TERM "$(cat "${jupyter_pidfile}")"

    echo "Stopping ${name}."
    kill -s TERM "${jupyter_pid}"

    rm ${jupyter_pidfile}
}

jupyter_status()
{
    # Try its best to find the service's status
    if [ -f "${jupyter_pidfile}" ]
    then
        jupyter_pid="$(cat "${jupyter_pidfile}")"
    fi

    if [ -z "${jupyter_pid}" ]
    then
    jupyter_pid=$(pgrep -f "jupyter-notebook")
    [ -n "${jupyter_pid}" ] && echo "${jupyter_pid}" > "${jupyter_pidfile}"
    fi

    if [ -n "${jupyter_pid}" ]
    then
        echo "${name} running with pid: $jupyter_pid"
    else
        echo "${name} not running? (pid not found)"
    fi
}

command_args=" >/dev/null 2>&1 &"

load_rc_config $name
run_rc_command "$1"
EOT

chmod +x /usr/local/jails/jupyter/usr/local/etc/rc.d/jupyter
service -j jupyter jupyter enable


####
# install further packages not through pip but packages
pkg -j jupyter install py38-plotly py-pandas  # ...
