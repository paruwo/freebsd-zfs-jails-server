### 1) ZFS
# zfs destroy jails/grafana
zfs create -o mountpoint=/usr/local/jails/grafana jails/grafana
zfs set quota=10G jails/grafana
tar -xpf /usr/local/jails/media/13.1/base.txz -C /usr/local/jails/grafana


### 2) put to /etc/jail.conf
cat <<EOT >> /etc/jail.conf

grafana {
  ip4.addr="192.168.0.11";
};
EOT

# only if poudriere active, then do modify repos:
mkdir -p /usr/local/jails/grafana/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/grafana/usr/local/etc/pkg/repos/poudriere.conf
poudriere: {
    url             : "http://192.168.0.13:8080/packages/130amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

# in doubt set "enabled: no"
# vim /usr/local/jails/grafana/etc/pkg/FreeBSD.conf


cat <<EOT >> /etc/hosts
192.168.0.11 grafana grafana.fritz.box
EOT




### 3) basic jail setup
cp /etc/resolv.conf /usr/local/jails/grafana/etc/
cp /etc/localtime /usr/local/jails/grafana/etc/
touch /usr/local/jails/grafana/etc/fstab

service jail start grafana

sysrc -j grafana sendmail_enable="NO"
sysrc -j grafana sendmail_submit_enable="NO"
sysrc -j grafana sendmail_outbound_enable="NO"
sysrc -j grafana sendmail_msp_queue_enable="NO"

cat <<EOT >> /etc/pf.conf
rdr on $ext_if proto tcp from any to any port 3000 -> 192.168.0.11 port 3000
EOT

service pf restart

cpuset -j grafana -cl 4
rctl jail:grafana:vmemoryuse:deny=2G



### 4) app setup
pkg -4 -j grafana install grafana8 vim-tiny collectd5
service -j grafana grafana enable
service -j grafana grafana start


# TODO setup collectd
# enable+configure grafana and network
# also set FQDNLookup to false
# vim /usr/local/jails/grafana/usr/local/etc/collectd.conf
