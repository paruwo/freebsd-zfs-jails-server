# It's not 100% clear why local package caching does not work, even if we use it through host's pkg tooling.
# This one mitigates network load through system and pkg caching.

# NOT NEEDED WITH HAVING 100% LOCAL PACKAGE BUILD THROUGH POUDRIERE IN PLACE

### 1) ZFS
zfs create -o mountpoint=/usr/local/jails/cache -o quota=10G jails/cache
tar -xpf /usr/local/jails/media/13.1/base.txz -C /usr/local/jails/cache



### 2) put to /etc/jail.conf
cat <<EOT >> /etc/jail.conf

cache {
  ip4.addr="192.168.0.5";
};
EOT


### 3) basic jail setup
cp /etc/resolv.conf /usr/local/jails/cache/etc/
cp /etc/localtime /usr/local/jails/cache/etc/
touch /usr/local/jails/cache/etc/fstab

service jail start cache

sysrc -j cache sendmail_enable="NO"
sysrc -j cache sendmail_submit_enable="NO"
sysrc -j cache sendmail_outbound_enable="NO"
sysrc -j cache sendmail_msp_queue_enable="NO"

cpuset -j cache -cl 4
rctl jail:cache:vmemoryuse:deny=1G



### 4) app setup
pkg -4 -j cache install squid vim-tiny squidview
jexec -l cache service squid enable


vim /usr/local/jails/cache/usr/local/etc/squid/squid.conf

visible_hostname <servername>
# no authentication, just don't restrict, it's just my cache
http_access allow all

# https://github.com/freebsd/pkg/issues/1181
# match against download urls for specific packages - their content never changes for the same url, so we cache aggressively
refresh_pattern -i (quarterly|latest)\/All\/.*(\.txz) 1440 100% 1051200 ignore-private ignore-must-revalidate override-expire ignore-no-cache
# match against meta-information - this shouldn't be cached quite so aggressively
refresh_pattern -i (quarterly|latest)\/.*(\.txz|\.pkg) 1440 100% 40080 ignore-private ignore-must-revalidate override-expire ignore-no-cache
## old existing entries must occur below ours...
# refresh_pattern ^ftp:       1440    20% 1008000
# ...

maximum_object_size_in_memory 1 MB
maximum_object_size 1000 MB



# create cache directories in /var/squid/cache
jexec -l cache squid -z

# finally, do start squid
jexec -l cache service squid start

# check that it works
# ... in different session
tail -f /usr/local/jails/cache/var/log/squid/access.log



### 6) set for other jails to benefit from pkg caching
# of course not for "cache", but maybe conceptually also not right
# echo 'pkg_env: { HTTP_PROXY: "http://192.168.0.5:3128", HTTPS_PROXY: "http://192.168.0.5:3128"}' > /usr/local/jails/postgresql/usr/local/etc/pkg.conf
# echo 'pkg_env: { HTTP_PROXY: "http://192.168.0.5:3128", HTTPS_PROXY: "http://192.168.0.5:3128"}' > /usr/local/jails/redis/usr/local/etc/pkg.conf
# echo 'pkg_env: { HTTP_PROXY: "http://192.168.0.5:3128", HTTPS_PROXY: "http://192.168.0.5:3128"}' > /usr/local/jails/grafana/usr/local/etc/pkg.conf
# echo 'pkg_env: { HTTP_PROXY: "http://192.168.0.5:3128", HTTPS_PROXY: "http://192.168.0.5:3128"}' > /usr/local/jails/influxdb/usr/local/etc/pkg.conf
# echo 'pkg_env: { HTTP_PROXY: "http://192.168.0.5:3128", HTTPS_PROXY: "http://192.168.0.5:3128"}' > /usr/local/jails/jenkins/usr/local/etc/pkg.conf
# echo 'pkg_env: { HTTP_PROXY: "http://192.168.0.5:3128", HTTPS_PROXY: "http://192.168.0.5:3128"}' > /usr/local/jails/jenkins-agent/usr/local/etc/pkg.conf
# echo 'pkg_env: { HTTP_PROXY: "http://192.168.0.5:3128", HTTPS_PROXY: "http://192.168.0.5:3128"}' > /usr/local/jails/kafka/usr/local/etc/pkg.conf
# maybe also localhost
# echo 'pkg_env: { HTTP_PROXY: "http://192.168.0.5:3128", HTTPS_PROXY: "http://192.168.0.5:3128"}' > /usr/local/etc/pkg.conf

## test if needed
#pkg update -f
#pkg install libxml2
#pkg delete libxml2
#pkg clean
## now we should see no more TCP_MISS in log
#pkg install libxml2
## option 1:
# jexec -l cache squidview
## option 2:
# jexec -l influxdb login -f root
# squidclient mgr:info

# TODO make it work, currently TCP_DENIED
# https://askubuntu.com/questions/1051554/squid-tcp-denied-403-4037-get-http-detectportal-firefox-com-success-txt-hier




## -1) decommisioning
service jail stop cache
zfs destroy jails/cache

# remove "cache" from /etc/rc.conf property jail_list
# remove "cache" from /etc/jails.conf
# (nothing to remove "cache" from /etc/pf.conf)

rm /usr/local/jails/postgresql/usr/local/etc/pkg.conf
rm /usr/local/jails/redis/usr/local/etc/pkg.conf
rm /usr/local/jails/grafana/usr/local/etc/pkg.conf
rm /usr/local/jails/influxdb/usr/local/etc/pkg.conf
rm /usr/local/jails/jenkins/usr/local/etc/pkg.conf
rm /usr/local/jails/jenkins-agent/usr/local/etc/pkg.conf
rm /usr/local/jails/kafka/usr/local/etc/pkg.conf
