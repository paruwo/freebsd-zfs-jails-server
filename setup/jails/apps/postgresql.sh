# https://www.postgresql.org/docs/13/monitoring-stats.html

### 1) ZFS
zfs create -o mountpoint=/usr/local/jails/postgresql -o quota=55G zroot/jails/postgresql
zfs create -o canmount=off -o compression=lz4 -o atime=off -o quota=50G zroot/jails/postgresql/data

tar -xpf /usr/local/jails/media/14.0/base.txz -C /usr/local/jails/postgresql



### 2) put to /etc/jail.conf
cat <<EOT >> /etc/jail.conf

postgresql {
  ip4.addr="192.168.0.2";
  # needed to get database running
  sysvmsg=new;
  sysvsem=new;
  sysvshm=new;
};
EOT


# only if poudriere active, then do modify repos:
mkdir -p /usr/local/jails/postgresql/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/postgresql/usr/local/etc/pkg/repos/poudriere.conf
poudriere: {
    url             : "http://192.168.0.13:8080/packages/130amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

# in doubt set "enabled: no"
# vim /usr/local/jails/postgresql/etc/pkg/FreeBSD.conf

cat <<EOT >> /etc/hosts
192.168.0.2 postgresql postgresql.fritz.box
EOT





### 3) basic jail setup
cp /etc/resolv.conf /usr/local/jails/postgresql/etc/
cp /etc/localtime /usr/local/jails/postgresql/etc/
touch /usr/local/jails/postgresql/etc/fstab

service jail restart postgresql

sysrc -j postgresql sendmail_enable="NO"
sysrc -j postgresql sendmail_submit_enable="NO"
sysrc -j postgresql sendmail_outbound_enable="NO"
sysrc -j postgresql sendmail_msp_queue_enable="NO"

cpuset -j postgresql -cl 0,1
rctl jail:postgresql:vmemoryuse:deny=2G

cat "rdr on $ext_if proto tcp from any to any port 5432 -> 192.168.0.2 port 5432" >> /etc/pf.conf
service pf restart



# optional collectd (take own build with postgres enabled)
pkg -4 -j postgresql install collectd5

# set FQDNLookup=false, set hostname, activate and configure postgres plugin
vim /usr/local/jails/postgresql/usr/local/etc/collectd.conf
# configure network: 192.168.0.10
# configure postgresql: databases weather + airflow + postgres
# configure cpu: ReportByState true, ReportByCpu true, ValuesPercentage	true, ReportNumCpu

service -j postgresql collectd enable
service -j postgresql collectd start
# for errors check logs in
# tail /usr/local/jails/postgresql/var/log/messages




### 4) app setup
pkg -4 -j postgresql install postgresql16-server postgresql16-contrib vim-tiny
service -j postgresql postgresql enable

# init db initially
jexec -l postgresql /usr/local/etc/rc.d/postgresql oneinitdb

# rename data and wal locations to moved to separate datasets
mv /usr/local/jails/postgresql/var/db/postgres/data16/pg_wal /usr/local/jails/postgresql/var/db/postgres/data16/pg_wal-old
mv /usr/local/jails/postgresql/var/db/postgres/data16/base /usr/local/jails/postgresql/var/db/postgres/data16/base-old

# create new datasets for data and wal
zfs create -o recordsize=8k -o redundant_metadata=most -o primarycache=metadata -o mountpoint=/usr/local/jails/postgresql/var/db/postgres/data16/pg_wal -o logbias=throughput zroot/jails/postgresql/data/pg_wal
zfs create -o recordsize=8k -o redundant_metadata=most -o primarycache=metadata -o mountpoint=/usr/local/jails/postgresql/var/db/postgres/data16/base -o logbias=throughput zroot/jails/postgresql/data/base

# copy back stuff
cp -Rp /usr/local/jails/postgresql/var/db/postgres/data16/pg_wal-old/* /usr/local/jails/postgresql/var/db/postgres/data16/pg_wal
cp -Rp /usr/local/jails/postgresql/var/db/postgres/data16/base-old/* /usr/local/jails/postgresql/var/db/postgres/data16/base
rm -rf /usr/local/jails/postgresql/var/db/postgres/data16/pg_wal-old
rm -rf /usr/local/jails/postgresql/var/db/postgres/data16/base-old

jexec -l postgresql chown -R postgres:postgres /var/db/postgres/data16/
service -j postgresql postgresql start


# setup connectivity
jexec -l postgresql login -f postgres
psql
   ALTER USER postgres WITH ENCRYPTED PASSWORD 'postgres';
   \q
echo 'host all all 192.168.0.0/16 md5' >> /var/db/postgres/data16/pg_hba.conf

vim data16/postgresql.conf
# switch "full_page_writes=off"

service postgresql restart
exit



# SSL
jexec -l postgresql login -f root

cd /var/db/postgres/data16/
openssl req -new -x509 -nodes -text -out server.crt -keyout server.key -subj "/CN=server.fritz.box"
chmod og-rwx server.key

# switch "ssl=on"
vim postgresql.conf

chown postgres:postgres server.*
service postgresql restart
exit



# benchmark
# we benchmark directly on server because client is installed, this is not 100% serious
jexec -l postgresql login -f postgres

# small set
# createdb weather
pgbench -i weather

# select only, 2 clients, 20 seconds on database weather: 60k TPS
pgbench -S -c 2 -M prepared -T 20 weather
# mixed read+write: 100 TPS (sic, not K or M)
pgbench -c 2 -M prepared -T 20 weather

# bigger data set
pgbench -i -s 100 weather

# cleanup
psql weather -c "drop table pgbench_accounts"
psql weather -c "drop table pgbench_branches"
psql weather -c "drop table pgbench_history"
psql weather -c "drop table pgbench_tellers"
# dropdb weather



# further helpers

# get database server version
jexec -l postgresql psql -V

# login to database as postgres user
jexec -l postgresql psql -U postgres
