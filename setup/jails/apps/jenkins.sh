### 1) ZFS
# zfs destroy jails/jenkins
zfs create -o mountpoint=/usr/local/jails/jenkins -o quota=10G jails/jenkins
tar -xpf /usr/local/jails/media/13.1/base.txz -C /usr/local/jails/jenkins



### 2) put to /etc/jail.conf
cat <<EOT >> /etc/jail.conf

jenkins {
  ip4.addr="192.168.0.3";
};
EOT

# only if poudriere active, then do modify repos:
mkdir -p /usr/local/jails/jenkins/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/jenkins/usr/local/etc/pkg/repos/poudriere.conf
poudriere: {
    url             : "http://192.168.0.13:8080/packages/130amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

# in doubt set "enabled: no"
# vim /usr/local/jails/jenkins/etc/pkg/FreeBSD.conf


cat <<EOT >> /etc/hosts
192.168.0.3 jenkins jenkins.fritz.box
EOT




### 3) basic jail setup
cp /etc/resolv.conf /usr/local/jails/jenkins/etc/
cp /etc/localtime /usr/local/jails/jenkins/etc/
touch /usr/local/jails/jenkins/etc/fstab

service jail restart jenkins

sysrc -j jenkins sendmail_enable="NO"
sysrc -j jenkins sendmail_submit_enable="NO"
sysrc -j jenkins sendmail_outbound_enable="NO"
sysrc -j jenkins sendmail_msp_queue_enable="NO"

# do heavy work on agents only, so minimize requirements here to 1 CPU, 2GB RAM, 10G storage
cpuset -j jenkins -cl 2
rctl jail:jenkins:vmemoryuse:deny=2G
zfs set quota=10G jails/jenkins

echo "rdr on $ext_if proto tcp from any to any port 8081 -> 192.168.0.3 port 8080" >> /etc/pf.conf
service pf restart



### 4) app setup
pkg -4 -j jenkins install jenkins-lts vim-tiny
vim /usr/local/jails/jenkins/usr/local/etc/rc.d/jenkins
# add ': ${jenkins_java_opts="-Xms512M -Xmx2G"}'

service -j jenkins jenkins enable
service -j jenkins jenkins onestart

jexec -l jenkins cat /usr/local/jenkins/secrets/initialAdminPassword
# get token and enter in www frontend http://<server:8180> and go on with plugins

# You may want to install SSH Agent.
