sysrc zfs_enable="YES"

# create basic striped pool (just volatile data, nothing for long-term)
zpool create -o ashift=12 -m mountpoint=/usr/local/jails jails /dev/ada3 /dev/ada4

mkdir -p /usr/local/jails/media/14.0
cd /usr/local/jails/media/14.0
curl https://download.freebsd.org/ftp/releases/amd64/amd64/14.0-RELEASE/base.txz --output base.txz
# curl https://download.freebsd.org/ftp/releases/amd64/amd64/14.0-RELEASE/ports.txz --output ports.txz

# if dependencies exist, do stop in reverse order
sysrc jail_reverse_stop=YES
