#!/bin/sh

if [ -z "$1" ]; then
   echo "no jail name given, checking jail versions"

   for j in `jls jid`; do
      echo "jail $j `jls -j $j name`, version `jexec -l $j freebsd-version`"
   done

   exit 2
else
  echo "updating jail $1"
  # freebsd-update -f /etc/jail-update.conf -b /usr/local/jails/$1 --currently-running `jexec -l $1 freebsd-version` fetch install
  freebsd-update -b /usr/local/jails/$1 --currently-running `jexec -l $1 freebsd-version` fetch install
  service jail restart $1
fi

exit 0
