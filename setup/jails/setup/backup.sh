# setup jail backup once

# work on SERVER, move snapshots to NAS
# zfs list -t snapshot

# NAS
ssh <user>@192.168.178.67
pw user add replicator -m
zfs allow -u replicator compression,mountpoint,create,mount,receive,quota hddpool/jail_backup
chown replicator:replicator /hddpool/jail_backup
sysctl vfs.usermount=1

# only backup what may have some value
zfs create -o mountpoint=/hddpool/jail_backup -o compression=lz4 -o quota=100G hddpool/jail_backup
zfs create -o mountpoint=/hddpool/jail_backup/postgresql hddpool/jail_backup/postgresql
zfs create -o mountpoint=/hddpool/jail_backup/jenkins hddpool/jail_backup/jenkins



# SERVER
pkg -4 install pv
pw user add replicator -m
passwd replicator  # set to empty
zfs allow -u replicator send,snapshot,hold,destroy,mount jails
su - replicator

cd .ssh
ssh-keygen -f id_rsa -t ecdsa -b 521
ssh-copy-id -i id_rsa.pub replicator@192.168.178.67
# alternatively
# cat id_rsa.pub | ssh replicator@192.168.178.67  'cat >> .ssh/authorized_keys'
cd ..

# test password-less access
ssh replicator@192.168.178.67



# EXAMPLE
zfs snapshot -r jails/redis@now
zfs send -R jails/redis@now | pv | ssh 192.168.178.67 zfs receive -F hddpool/jail_backup/redis
zfs destroy jails/redis@now
