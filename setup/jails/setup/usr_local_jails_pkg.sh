#!/bin/sh

# no parameters

pkg update -f

for j in $(jls -h name);
do
   echo "updating $j..."
   ASSUME_ALWAYS_YES=yes pkg -4 -j "$j" upgrade
done
