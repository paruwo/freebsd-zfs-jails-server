#!/bin/sh
# create and export snapshots for all jails to NAS - user = replicator

dt=$(date +'%Y-%m-%d')

for jailname in `jls name`; do
  echo "backing up ${jailname}"
  zfs snapshot -r jails/${jailname}@${dt}
  zfs send -R jails/${jailname}@${dt} | pv | ssh 192.168.178.67 zfs receive -F hddpool/jail_backup/${jailname}
  zfs destroy jails/${jailname}@${dt}
done
