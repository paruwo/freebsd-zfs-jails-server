# just a bunch of more helpful snippets

# manage which jails to start on boot, here for instance these:
sysrc jail_list="jenkins jenkins-agent postgresql"

# run monthly on demand
zpool scrub jails
zpool scrub backuppool
# check scrubbing status
zpool status <zfspoolname>


# login to jail as root
jexec -l <jailname> login -f root

# execute single command in jail
jexec -l <jailname> freebsd-version
# .. as different user
jexec -l -U <user> <jailname> freebsd-version

# check network
sockstat -j <jailname> -4

# update jail base system
/usr/local/jails/jailpatch.sh <jailname>
# update jail packages
pkg -4 -j <jailname> upgrade



# update jail system (/usr/local/jails/upgrade-jail.sh)
freebsd-update -f /etc/jail-update.conf -b /usr/local/jails/<j> \
   --currently-running `jexec -l <j> freebsd-version` fetch install

freebsd-update -f /etc/jail-update.conf -b /usr/local/jails/<j> \
   --currently-running `jexec -l <j> freebsd-version` -r 13.1-RELEASE upgrade

freebsd-update -f /etc/jail-update.conf -b /usr/local/jails/<j> \
   --currently-running `jexec -l <j> freebsd-version` -r 13.1-RELEASE install

# don't ask why but restricting to IPV4 seems to improve download performance
pkg -4 -j <j> upgrade -fy

freebsd-update -f /etc/jail-update.conf -b /usr/local/jails/<j> \
   --currently-running `jexec -l <j> freebsd-version` -r 13.1-RELEASE install

service jail restart <j>
