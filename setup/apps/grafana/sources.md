### Data Source / collectd

 - type: InfluxDB
 - name: collectd
 - url: https://192.168.0.10:8086
 - auth: skip TLS verify
 - database: collectd

### Data Source / weather

 - type: InfluxDB
 - name: weather
 - url: https://192.168.0.10:8086
 - auth: skip TLS verify
 - database: weather

### Data Source / Weather

 - type: Postgresql
 - name: Weather
 - host: 192.168.0.2
 - database: weather
 - user: weather
 - TSL/SSL: require
 - Version: 12+
 
### Data Source / internal InfluxDB

Feel free to use Grafana dashboard with id #421 for visualization.
Just ensure that _internal database is active and that the grafana source was configured.
Then import the dashboard, open it and set the appropriate datasource in all panels. 

 - type: InfluxDB
 - name: InfluxDB-internal
 - url: https://192.168.0.10:8086
 - auth: skip TLS verify
 - database: internal
