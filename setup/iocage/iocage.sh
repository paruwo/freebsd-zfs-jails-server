# only execute in case of IOCAGE _instead_ of plain jails

# just to ensure - check both
# check that fdescfs is configured in etc/fstab
# sysrc zfs_enable="YES"


# create basic striped pool (just volatile data, nothing for long-term)
zpool create -o ashift=12 jails /dev/ada3 /dev/ada4
zfs set mountpoint=/usr/local/jails jails


# enable official FreeBSD repo and install from there if you face ruby build issues
pkg install sysutils/iocage

# creates and acquires jails/iocage/*
iocage activate jails
iocage fetch
service iocage onestart


# optionally create TEST JAIL and bind to any of the known reserved CIDR
iocage create -n test ip4_addr="lo1|192.168.0.32/32" -r 13.1-RELEASE
iocage start test
iocage pkg test install nginx
iocage console test
service nginx onestart
# add to pf.conf and restart once
# rdr on $ext_if proto tcp from any to any port 3280 -> 192.168.0.32 port 80
# service pf restart
# connect browser to server.fritz.box:3280 should work
# cleanup
iocage destroy -f test
# remove rdr entry from above and then run "service pf restart" again


# ------------------------------------
# if all does work, then enable iocage
service iocage enable
