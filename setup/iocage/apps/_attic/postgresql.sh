
# THIS SETUP IS DEPRECATED
exit

# Initial setup only, see jail postgresql14 for major database upgrade

iocage create -T -n postgresql ip4_addr="lo1|192.168.0.2" -r 13.1-RELEASE
zfs create -o canmount=off -o compression=lz4 -o atime=off -o quota=50G jails/iocage/jails/postgresql/root/data
iocage set boot=1 postgresql
iocage set quota=55G postgresql
iocage set sysvmsg=new postgresql
iocage set sysvsem=new postgresql
iocage set sysvshm=new postgresql
iocage set vmemoryuse="deny=2G" postgresql
# iocage set cpuset=0,1 postgresql

cat "rdr on $ext_if proto tcp from any to any port 5432 -> 192.168.0.2 port 5432" >> /etc/pf.conf
service pf restart


cat <<EOT >> /etc/hosts
192.168.0.2 postgresql postgresql.fritz.box
EOT


# only if poudriere active, then do modify repos:
mkdir -p /usr/local/jails/iocage/jails/postgresql/root/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/iocage/jails/postgresql/root/usr/local/etc/pkg/repos/poudriere.conf
poudriere131: {
    url             : "http://192.168.0.13:8080/packages/131amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

# .. and disable official repo if you are going to use it
cat <<EOT >> /usr/local/jails/iocage/jails/postgresql/root/usr/local/etc/pkg/repos/freebsd.conf
FreeBSD: {
    enabled: no
}
EOT


iocage start postgresql
iocage pkg postgresql update
iocage pkg postgresql install postgresql13-server postgresql13-contrib vim-tiny collectd5
iocage exec postgresql service postgresql enable



# set FQDNLookup=false, set hostname, activate and configure postgres plugin
vim /usr/local/jails/iocage/jails/postgresql/root/usr/local/etc/collectd.conf
# enable: df, disk, processes, swap, uptime
# configure network: 192.168.0.10
# configure postgresql: databases weather + airflow + postgres
# configure cpu: ReportByState true, ReportByCpu true, ValuesPercentage	true, ReportNumCpu

iocage console postgresql
service collectd enable
service collectd start
# for errors check logs in
# tail /usr/local/jails/iocage/jails/postgresql/root/var/log/messages
exit


# init db initially
iocage exec postgresql /usr/local/etc/rc.d/postgresql oneinitdb

# rename data and wal locations to moved to separate datasets
mv /usr/local/jails/iocage/jails/postgresql/root/var/db/postgres/data13/pg_wal /usr/local/jails/iocage/jails/postgresql/root/var/db/postgres/data13/pg_wal-old
mv /usr/local/jails/iocage/jails/postgresql/root/var/db/postgres/data13/base /usr/local/jails/iocage/jails/postgresql/root/var/db/postgres/data13/base-old


# create new datasets for data and wal
zfs create -o recordsize=8k -o redundant_metadata=most -o primarycache=metadata -o mountpoint=/usr/local/jails/iocage/jails/postgresql/root/var/db/postgres/data13/pg_wal -o logbias=throughput jails/iocage/jails/postgresql/root/data/pg_wal
zfs create -o recordsize=8k -o redundant_metadata=most -o primarycache=metadata -o mountpoint=/usr/local/jails/iocage/jails/postgresql/root/var/db/postgres/data13/base -o logbias=throughput jails/iocage/jails/postgresql/root/data/base

# copy back stuff
cp -Rp /usr/local/jails/iocage/jails/postgresql/root/var/db/postgres/data13/pg_wal-old/* /usr/local/jails/iocage/jails/postgresql/root/var/db/postgres/data13/pg_wal
cp -Rp /usr/local/jails/iocage/jails/postgresql/root/var/db/postgres/data13/base-old/* /usr/local/jails/iocage/jails/postgresql/root/var/db/postgres/data13/base
rm -rf /usr/local/jails/iocage/jails/postgresql/root/var/db/postgres/data13/pg_wal-old
rm -rf /usr/local/jails/iocage/jails/postgresql/root/var/db/postgres/data13/base-old



iocage exec postgresql chown -R postgres:postgres /var/db/postgres/data13/
iocage exec postgresql service postgresql onestart

# setup connectivity
iocage exec postgresql
su - postgres
psql
   ALTER USER postgres WITH ENCRYPTED PASSWORD '...';
   \q
echo 'host all all 192.168.0.0/16 md5' >> /var/db/postgres/data13/pg_hba.conf

vim data13/postgresql.conf
# switch "full_page_writes=off"

service postgresql restart
exit
exit  # to host



# SSL
iocage exec postgresql

cd /var/db/postgres/data13/
openssl req -new -x509 -nodes -text -out server.crt -keyout server.key -subj "/CN=server.fritz.box"
chmod og-rwx server.key

# switch "ssl=on"
vim postgresql.conf

chown postgres:postgres server.*
service postgresql restart
exit





# optional: benchmark
# we benchmark directly on server because client is installed, this is not 100% serious
iocage exec postgresql
su - postgres

# small set
pgbench -i weather

# select only, 2 clients, 20 seconds on database weather: 60k TPS
pgbench -S -c 2 -M prepared -T 20 weather
# mixed read+write: 100 TPS (sic, not K or M)
pgbench -c 2 -M prepared -T 20 weather

# bigger data set
pgbench -i -s 100 weather

# cleanup
psql weather -c "drop table pgbench_accounts"
psql weather -c "drop table pgbench_branches"
psql weather -c "drop table pgbench_history"
psql weather -c "drop table pgbench_tellers"
