
# not a thick jail, for bigger updates create new jail
iocage create -n weather ip4_addr="lo1|192.168.0.16" -r 13.1-RELEASE
iocage set quota=5G weather
iocage set boot=on weather
iocage set vmemoryuse="deny=1G" weather
# iocage set cpuset=7 weather


echo 'rdr on $ext_if proto tcp from any to any port 8016 -> 192.168.0.16 port 8080' >> /etc/pf.conf
service pf restart


cat <<EOT >> /etc/hosts
192.168.0.16 weather weather.fritz.box
EOT

## only if poudriere active, then do modify repos:
mkdir -p /usr/local/jails/iocage/jails/weather/root/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/iocage/jails/weather/root/usr/local/etc/pkg/repos/poudriere.conf
poudriere131: {
    url             : "http://192.168.0.13:8080/packages/131amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

cat <<EOT >> /usr/local/jails/iocage/jails/weather/root/usr/local/etc/pkg/repos/freebsd.conf
FreeBSD: {
    enabled: no
}
EOT



iocage start weather
iocage pkg weather update
iocage pkg weather install vim-tiny git-tiny python39 libxml2 libxslt postgresql14-client py39-sqlite3



# Postgres setup, only once if needed
iocage console postgresql
su - postgres
psql
CREATE USER weather WITH PASSWORD 'weather' CREATEDB;
CREATE DATABASE weather OWNER weather;
\q
exit



# app setup
iocage console weather
git clone https://codeberg.org/paruwo/Weather.git && cd Weather
python3.9 -m ensurepip
pip3 install poetry
poetry install --no-dev
poetry install --extras "postgres"

# app config
vim weather/config/weather.toml
# influxdb: endpoint=https://192.168.0.10:8086
# postgresql: host=192.168.0.214, port:5432


# install crontab for regular data refresh
# crontab -e
# @reboot cd /root/Weather && poetry run python Weather.py -m http -t rain air wind solar --full
# @hourly cd /root/Weather && poetry run python Weather.py -m http -t rain air wind solar --delta
# @reboot cd /root/Weather && poetry run python Weather.py -m http -t air --full --target postgresql
# @hourly cd /root/Weather && poetry run python Weather.py -m http -t air --delta --target postgresql
# (add empty line)


# one-time load of historic data to postgresql (takes 10+ minutes)
# precondition: deploy https://codeberg.org/paruwo/Weather/src/branch/master/database/postgresql/load_agg_air.sql
iocage console weather
cd Weather
poetry run python Weather.py -m http -t air --hist --target postgresql
poetry run python Weather.py -m http -t air --full --target postgresql
