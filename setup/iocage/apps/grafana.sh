
iocage create -T -n grafana ip4_addr="lo1|192.168.0.11" -r 13.1-RELEASE
iocage set quota=10G grafana
iocage set boot=1 grafana
iocage set depends="postgresql influxdb" grafana
iocage set vmemoryuse="deny=2G" grafana
# iocage set cpuset=4 grafana

cat <<EOT >> /etc/hosts
192.168.0.11 grafana grafana.fritz.box
EOT

echo 'rdr on $ext_if proto tcp from any to any port 3000 -> 192.168.0.11 port 3000' >> /etc/pf.conf
service pf restart


# only if poudriere active, then do modify repos:
mkdir -p /usr/local/jails/iocage/jails/grafana/root/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/iocage/jails/grafana/root/usr/local/etc/pkg/repos/poudriere.conf
poudriere131: {
    url             : "http://192.168.0.13:8080/packages/131amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

cat <<EOT >> /usr/local/jails/iocage/jails/grafana/root/usr/local/etc/pkg/repos/freebsd.conf
FreeBSD: {
    enabled: no
}
EOT




iocage start grafana

iocage pkg grafana update
iocage pkg grafana install grafana8 vim-tiny collectd5
iocage exec grafana service grafana enable
iocage exec grafana service grafana start



# (1) UPGRADE in-place
iocage pkg grafana update
iocage pkg grafana upgrade
iocage upgrade -r 13.1 grafana
