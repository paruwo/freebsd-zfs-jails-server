# https://dan.langille.org/2019/10/23/moving-poudriere-from-the-host-into-a-jail/

# ports location: /usr/local/jails/iocage/jails/poudriere/root/work/ports/default
# all packages:   /usr/local/jails/iocage/jails/poudriere/root/work/data/packages/130amd64-default/All/

# TODO also check https://danschmid.de/article/poudriere-guide/

########################################################################################################################


iocage create -T -n poudriere ip4_addr="lo1|192.168.0.13" -r 13.1-RELEASE
iocage set quota=100G poudriere
iocage set boot=on poudriere

iocage set allow_mount=1 poudriere
iocage set allow_mount_procfs=1 poudriere
iocage set allow_mount_zfs=1 poudriere
iocage set allow_mount_nullfs=1 poudriere
iocage set allow_mount_devfs=1 poudriere
iocage set allow_mount_tmpfs=1 poudriere
iocage set jail_zfs=1 poudriere
iocage set allow_raw_sockets=1 poudriere
iocage set allow_socket_af=1 poudriere
iocage set allow_chflags=1 poudriere
iocage set children_max=16 poudriere
iocage set enforce_statfs=1 poudriere



mkdir /usr/local/jails/iocage/jails/poudriere/root/distfiles


echo 'rdr on $ext_if proto tcp from any to any port 8100 -> 192.168.0.13 port 8080' >> /etc/pf.conf
service pf restart


cat <<EOT >> /etc/hosts
192.168.0.13 poudriere poudriere.fritz.box
EOT


# only if poudriere active, then (1) add local repo and (2) remove official repos to not mix up packages
mkdir -p /usr/local/jails/iocage/jails/poudriere/root/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/iocage/jails/poudriere/root/usr/local/etc/pkg/repos/poudriere.conf
poudriere131: {
    url             : "http://192.168.0.13:8080/packages/131amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 100
}
EOT

# of course, do only disable official repos when setup and builds were complete and repo does work
cat <<EOT >> /usr/local/jails/iocage/jails/poudriere/root/usr/local/etc/pkg/repos/freebsd.conf
FreeBSD: {
    enabled: no
}
EOT


iocage start poudriere
iocage pkg poudriere update
iocage pkg poudriere install collectd5 poudriere vim-tiny git-tiny ccache
iocage stop poudriere


# nested volume to get poudriere work using zfs
zfs create jails/iocage/jails/poudriere/root/work
iocage set jail_zfs_dataset=iocage/jails/poudriere/root/work poudriere
iocage start poudriere
iocage exec poudriere zfs set mountpoint=/work jails/iocage/jails/poudriere/root/work


### setup ccache (fills up and get used only after first build execution)
iocage exec poudriere mkdir -p /var/cache/ccache
cat <<EOT >> /usr/local/jails/iocage/jails/poudriere/root/var/cache/ccache/ccache.conf
max_size = 16GB
EOT


### setup collectd
# set FQDNLookup=false, set hostname
vim /usr/local/jails/iocage/jails/poudriere/root/usr/local/etc/collectd.conf
# enable: df, disk, processes, swap, uptime
# network: 192.168.0.10

iocage exec poudriere service collectd enable
iocage exec poudriere service collectd start
# for errors check logs in
# tail /usr/local/jails/postgresql/var/log/messages


### configure poudriere
# set the following
vim /usr/local/jails/iocage/jails/poudriere/root/usr/local/etc/poudriere.conf
# adjust / uncomment to have these values
#ZPOOL=jails
#ZROOTFS=/iocage/jails/poudriere/root/work
#FREEBSD_HOST=https://download.freebsd.org
#BASEFS=/work
#USE_TMPFS="all"
## MAX_MEMORY=4  # keep commented out, OpenJDK needs >4GB
#DISTFILES_CACHE=/distfiles
#CHECK_CHANGED_OPTIONS=verbose
#CCACHE_DIR=/var/cache/ccache
#ALLOW_NETWORKING_PACKAGES="python38 ruby"
#PARALLEL_JOBS=4
#NOLINUX=yes
#ALLOW_MAKE_JOBS_PACKAGES="pkg ccache py* llvm* gcc* pkg rust cmake"
#BUILDER_HOSTNAME="poudriere.fritz.box"
#PRIORITY_BOOST="pyp openoffice* llvm* rust openjdk*"


# default build options, taken from https://wissen.der-beweis.de/freebsd/makeconf_headless.html
# we assume packages targeting to headless system
cat <<EOT >> /usr/local/jails/iocage/jails/poudriere/root/usr/local/etc/poudriere.d/make.conf
# optimizations
CPUTYPE?=native
OPTIONS_SET=OPTIMIZED_CFLAGS CPUFLAGS
WITH_CCACHE_BUILD=yes

# LLVM is a long-runner, do not build for all architectures
WITHOUT_LLVM_TARGET_ALL=

# globally disable these options
OPTIONS_UNSET+= DOCS NLS X11 EXAMPLES CUPS DEBUG FONTCONFIG TEST IPV6
EOT


# setup nested build jail and clone ports
# if needed, one could setup multiple nested jails of different versions
# iocage exec poudriere poudriere jail -c -j 130amd64 -v 13.0-RELEASE
iocage exec poudriere poudriere jail -c -j 131amd64 -v 13.1-RELEASE
iocage exec poudriere poudriere ports -c




### www frontend
iocage pkg poudriere install nginx

vim /usr/local/jails/iocage/jails/poudriere/root/usr/local/etc/nginx/nginx.conf
# add this as last section of main http block:
# # virtual hosting
# include /usr/local/etc/nginx/vhosts/*;

mkdir -p /usr/local/jails/iocage/jails/poudriere/root/usr/local/etc/nginx/vhosts/
cat <<EOT >> /usr/local/jails/iocage/jails/poudriere/root/usr/local/etc/nginx/vhosts/poudriere.conf
server {
  listen 8080 default;
  server_name poudriere.lan;
  root /usr/local/share/poudriere/html;

  location /data {
    alias /work/data/logs/bulk;
    autoindex on;
  }

  location /packages {
    root /work/data;
    autoindex on;
  }
}
EOT

iocage exec poudriere chown nobody:nobody /usr/local/etc/nginx/vhosts/poudriere.conf
iocage exec poudriere service nginx enable
iocage exec poudriere service nginx restart

vim /usr/local/jails/iocage/jails/poudriere/root/usr/local/etc/nginx/mime.types
# modify
# text/plain                          txt log;"

# for testing, call in browser <server>:8100 (routed through pf)





### 7) regular tasks
# maintain list of ports to build, for instance
cat <<EOT >> /usr/local/jails/iocage/jails/poudriere/root/usr/local/etc/package-list.txt
benchmarks/iperf

databases/influxdb
databases/postgresql14-server
databases/postgresql14-contrib
databases/postgresql14-client
databases/py-sqlite3@py38
databases/py-sqlite3@py39
databases/py-sqlite3@py310
databases/py-sqlite3@py311
databases/redis
databases/sqlite3

devel/ccache
devel/git@tiny
devel/icu
devel/jenkins-lts
devel/py-virtualenv
devel/zookeeper

editors/vim
editors/vim@tiny

ftp/curl
ftp/lftp

java/openjdk8
java/openjdk11

lang/python38
lang/python39
lang/python310
lang/python311
lang/tcl86

# net/kafka
# net/kcat

net-mgmt/collectd5

ports-mgmt/poudriere

security/doas

shells/bash
shells/zsh
shells/ohmyzsh

sysutils/containerd
sysutils/grub2-bhyve
sysutils/vm-bhyve
sysutils/iocage
sysutils/tmux

textproc/libxml2
textproc/libxslt

www/grafana8
www/nginx
www/py-notebook
EOT


# simple reconfigurations
#    choose +pgsql, +python, +redis, +zookeeper
iocage exec poudriere poudriere options -c -n -j 131amd64 net-mgmt/collectd5
#    chose -rdoc
iocage exec poudriere poudriere options -c -n -j 131amd64 lang/ruby27




#### regular work

# update does not work to old way, so we have to login
iocage console poudriere
# iocage pkg poudriere update && iocage pkg poudriere upgrade
poudriere ports -u -v
poudriere jail -u -j 131amd64
exit



# We are also free to run huge packages separately and consequentially, however PRIORITY_BOOST is set.
# ...on all 8 cores of AMD FX-8350, 16GB RAM, striped dual HDD disk setup
# ...where all further jails were stopped
#
# all need 4GB+ RAM per builder
# takes 45-55min (consumes 4GB swap additionally)
/usr/bin/nice -n 15 iocage exec poudriere poudriere bulk -j 131amd64 lang/gcc10
# takes 45-90min
# /usr/bin/nice -n 15 iocage exec poudriere poudriere bulk -j 131amd64 lang/rust
# takes 20-30min
/usr/bin/nice -n 15 iocage exec poudriere poudriere bulk -j 131amd64 java/openjdk8
/usr/bin/nice -n 15 iocage exec poudriere poudriere bulk -j 131amd64 java/openjdk11
# takes 45-55min
# LLVM13 is special and may break with "out of memory", definitely needs 32GB memory (RAM+SWAP)
# demands MAX_MEMORY to be comment out / set to none in
# /usr/local/jails/iocage/jails/poudriere/root/usr/local/etc/poudriere.conf
set MAKE_JOBS_UNSAFE=yes
/usr/bin/nice -n 15 iocage exec poudriere poudriere bulk -j 131amd64 devel/llvm13


# build all (also here: be nice)
iocage console poudriere
/usr/bin/nice -n 15 poudriere bulk -j 131amd64 -f /usr/local/etc/package-list.txt
exit



# update server and all jails
# iocage pkg <jail> update && iocage pkg <jail> upgrade
pkg upgrade
iocage pkg poudriere upgrade
iocage pkg grafana upgrade && iocage exec grafana service grafana restart
iocage pkg influxdb upgrade && iocage exec influxdb service influxd restart
iocage pkg postgresql14 upgrade && iocage exec postgresql14 service postgresql restart
iocage pkg weather upgrade
iocage pkg jenkins-agent upgrade
iocage pkg jenkins upgrade && iocage exec jenkins service jenkins restart
# iocage console jenkins-agent-linux
#   # apt-get update && apt-get upgrade && apt-get dist-upgrade && exit
#
# iocage start airflow
# iocage pkg airflow upgrade && iocage pkg redis upgrade
# iocage stop airflow && iocage stop redis



# clean all explicitly built ports
# iocage exec poudriere poudriere bulk -C -j 131amd64 -f /usr/local/etc/package-list.txt

# check cache efficiency
# env CCACHE_DIR=/var/cache/ccache ccache -s
iocage exec poudriere env CCACHE_DIR=/var/cache/ccache ccache -s

# clean unused packages
iocage exec poudriere poudriere pkgclean -j 131amd64 -f /usr/local/etc/package-list.txt
