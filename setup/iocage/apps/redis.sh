
# not a thick jail, for bigger updates create new jail
iocage create -n redis ip4_addr="lo1|192.168.0.6" -r 13.1-RELEASE
iocage set quota=10G redis
iocage set vmemoryuse="deny=1G" redis
# iocage set cpuset=5 redis

echo 'rdr on $ext_if proto tcp from any to any port 6379 -> 192.168.0.6 port 6379' >> /etc/pf.conf
service pf restart


cat <<EOT >> /etc/hosts
192.168.0.6 redis redis.fritz.box
EOT


# only if poudriere active:
mkdir -p /usr/local/jails/iocage/jails/redis/root/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/iocage/jails/redis/root/usr/local/etc/pkg/repos/poudriere.conf
poudriere131: {
    url             : "http://192.168.0.13:8080/packages/131amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

cat <<EOT >> /usr/local/jails/iocage/jails/redis/root/usr/local/etc/pkg/repos/freebsd.conf
FreeBSD: {
    enabled: no
}
EOT



iocage start redis
iocage pkg redis update -f
iocage pkg redis install redis vim-tiny collectd5
iocage exec redis service redis enable
iocage exec redis service redis start


# to run it in this local setup we are good to go the easy way
# vim /usr/local/jails/iocage/jails/redis/root/usr/local/etc/redis.conf
# protected-mode no


### collectd
# set FQDNLookup=false, set hostname
# enable: df, disk, processes, redis, swap, uptime
vim /usr/local/jails/iocage/jails/redis/root/usr/local/etc/collectd.conf
# network: 192.168.0.10
# redis:
# <Plugin>
#   <Node "redis">
#      Host "192.168.0.6"
#      Port "6379"
#      Timeout 2000
#   </Node>
# </Plugin>

iocage exec redis service collectd enable
iocage exec redis service collectd start


# tiny benchmark
iocage exec redis redis-benchmark -h localhost -t set -n 10000000 -d 4 -c 8 -q -P 16

