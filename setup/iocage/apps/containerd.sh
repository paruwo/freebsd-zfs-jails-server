
# https://samuel.karp.dev/blog/2021/05/running-freebsd-jails-with-containerd-1-5/
# use this and setup with nested jails

# problem:
#  - uses legacy mounts (whatever this means)
#  - cannot unmount snapshots, so also cannot stop/restart jail easily
#  - not really ready for jailed execution (at least not at the first glance)

iocage create -T -n containerd ip4_addr="lo1|192.168.0.18" -r 13.1-RELEASE
iocage set quota=100G containerd
iocage set boot=0 containerd

iocage set allow_mount=1 containerd
iocage set allow_mount_zfs=1 containerd
iocage set allow_mount_devfs=1 poudriere
iocage set jail_zfs=1 containerd
iocage set children_max=16 containerd




cat <<EOT >> /etc/hosts
192.168.0.18 containerd containerd.fritz.box
EOT

# echo 'rdr on $ext_if proto tcp from any to any port xxxx -> 192.168.0.18 port xxxx' >> /etc/pf.conf
# service pf restart


# only if poudriere active, then do modify repos:
mkdir -p /usr/local/jails/iocage/jails/containerd/root/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/iocage/jails/containerd/root/usr/local/etc/pkg/repos/poudriere.conf
poudriere131: {
    url             : "http://192.168.0.13:8080/packages/131amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

cat <<EOT >> /usr/local/jails/iocage/jails/containerd/root/usr/local/etc/pkg/repos/freebsd.conf
FreeBSD: {
    enabled: no
}
EOT



# nested volume to get containerd work using zfs
zfs create jails/iocage/jails/containerd/root/containerd
iocage set jail_zfs_dataset=iocage/jails/containerd/root/containerd containerd
iocage start containerd
iocage exec containerd zfs set mountpoint=/var/lib/containerd/io.containerd.snapshotter.v1.zfs jails/iocage/jails/containerd/root/containerd

# install core apps
iocage pkg containerd update
iocage pkg containerd install containerd vim-tiny git-tiny go # collectd5 #later

# install helpers
iocage console containerd
cd ~ && git clone https://github.com/samuelkarp/runj
cd runj && make && make install

# create empty config
mkdir /etc/containerd/ && touch /etc/containerd/config.toml

service containerd enable
service containerd start


# first test
ctr image pull --snapshotter zfs public.ecr.aws/samuelkarp/freebsd:13.1-RELEASE
ctr run --snapshotter zfs --runtime wtf.sbk.runj.v1 --rm public.ecr.aws/samuelkarp/freebsd:13.1-RELEASE my-container-id sh -c 'echo "Hello from the container!"'

# to stop: need to destroy core parts
# zfs destroy -f -r jails/iocage/jails/containerd/root/containerd/1
