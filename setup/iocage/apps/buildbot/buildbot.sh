
# account: buildbot-master/buildbot
# taken from https://www.digitalocean.com/community/tutorials/how-to-install-buildbot-freebsd
# TODO test https://www.digitalocean.com/community/tutorials/how-to-build-and-deploy-packages-for-your-freebsd-servers-using-buildbot-and-poudriere

iocage create -T -n buildbot ip4_addr="lo1|192.168.0.20" -r 13.1-RELEASE
iocage set quota=10G buildbot
iocage set boot=0 buildbot
iocage set depends="buildbot-worker0" buildbot
iocage set vmemoryuse="deny=2G" buildbot

cat <<EOT >> /etc/hosts
192.168.0.20 buildbot buildbot.fritz.box
EOT

echo 'rdr on $ext_if proto tcp from any to any port 8010 -> 192.168.0.11 port 8010' >> /etc/pf.conf
service pf restart


# only if poudriere active, then do modify repos:
#mkdir -p /usr/local/jails/iocage/jails/buildbot/root/usr/local/etc/pkg/repos/
#cat <<EOT >> /usr/local/jails/iocage/jails/buildbot/root/usr/local/etc/pkg/repos/poudriere.conf
#poudriere131: {
#    url             : "http://192.168.0.13:8080/packages/131amd64-default/",
#    enabled         : yes,
#    mirror_type     : "http",
#    priority        : 10
#}
#EOT

#cat <<EOT >> /usr/local/jails/iocage/jails/buildbot/root/usr/local/etc/pkg/repos/freebsd.conf
#FreeBSD: {
#    enabled: no
#}
#EOT


# start and install
iocage start buildbot
iocage pkg buildbot install vim-tiny git collectd5 py38-buildbot py38-buildbot-www


# setup
iocage console buildbot

pw useradd -n buildbot-master -m
passwd buildbot-master
mkdir /var/buildbot-master
chown buildbot-master:buildbot-master /var/buildbot-master

su -l buildbot-master
buildbot-3.8 create-master /var/buildbot-master
cp /var/buildbot-master/master.cfg.sample /var/buildbot-master/master.cfg
exit
exit

vim /usr/local/jails/iocage/jails/buildbot/root/var/buildbot-master/master.cfg
# c['workers'] = [worker.Worker("worker0", "buildbot0")]
#       workernames=["worker0"],

iocage exec buildbot sysrc buildbot_basedir=/var/buildbot-master
iocage exec buildbot sysrc buildbot_user=buildbot-master
iocage exec buildbot sysrc buildbot_enable="YES"
iocage exec buildbot service buildbot start

# check
iocage exec buildbot tail /var/buildbot-master/twistd.log

