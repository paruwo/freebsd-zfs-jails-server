
# account: buildbot-master/buildbot
# taken from https://www.digitalocean.com/community/tutorials/how-to-install-buildbot-freebsd

iocage create -T -n buildbot-worker0 ip4_addr="lo1|192.168.0.21" -r 13.1-RELEASE
iocage set quota=50G buildbot-worker0
iocage set boot=0 buildbot-worker0
iocage set vmemoryuse="deny=2G" buildbot-worker0

cat <<EOT >> /etc/hosts
192.168.0.21 buildbot-worker0 buildbot-worker0.fritz.box
EOT


# only if poudriere active, then do modify repos:
#mkdir -p /usr/local/jails/iocage/jails/buildbot/root/usr/local/etc/pkg/repos/
#cat <<EOT >> /usr/local/jails/iocage/jails/buildbot/root/usr/local/etc/pkg/repos/poudriere.conf
#poudriere131: {
#    url             : "http://192.168.0.13:8080/packages/131amd64-default/",
#    enabled         : yes,
#    mirror_type     : "http",
#    priority        : 10
#}
#EOT

#cat <<EOT >> /usr/local/jails/iocage/jails/buildbot/root/usr/local/etc/pkg/repos/freebsd.conf
#FreeBSD: {
#    enabled: no
#}
#EOT


# start and install
iocage start buildbot-worker0
iocage pkg buildbot-worker0 install vim-tiny git collectd5 py38-buildbot-worker py38-twisted


# setup
iocage console buildbot-worker0

pw useradd -n buildbot-worker -m
passwd buildbot-worker
mkdir /var/buildbot-worker
chown buildbot-worker:buildbot-worker /var/buildbot-worker

su -l buildbot-worker
buildbot-worker-3.8 create-worker /var/buildbot-worker 192.168.0.20 worker0 'buildbot0'
echo 'Christian <me@home.town>' >/var/buildbot-worker/info/admin
echo 'worker0' >/var/buildbot-worker/info/host
exit
exit

iocage exec buildbot-worker0 sysrc buildbot_worker_basedir=/var/buildbot-worker
iocage exec buildbot-worker0 sysrc buildbot_worker_uid=buildbot-worker
iocage exec buildbot-worker0 sysrc buildbot_worker_gid=buildbot-worker
iocage exec buildbot-worker0 sysrc buildbot_worker_enable=YES

iocage exec buildbot-worker0 service buildbot-worker restart

# check logs to get "message from master: attached"
iocage exec buildbot-worker0 tail /var/buildbot-worker/twistd.log

