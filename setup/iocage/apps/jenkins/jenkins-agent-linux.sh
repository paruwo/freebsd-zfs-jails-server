
# https://iocage.readthedocs.io/en/latest/debian.html

# When using this agent in Jenkins, configure -Xmx2G in "Startup" -> "Extended" -> "JVM Options"

# empty jail
iocage create -T -e -n jenkins-agent-linux exec_start="/bin/true" exec_stop="/bin/true" ip4_addr="lo1|192.168.0.17"
iocage set quota=20G jenkins-agent-linux
iocage set boot=0 jenkins-agent-linux
iocage set vmemoryuse="deny=4G" jenkins-agent-linux

iocage set allow_mount=1 jenkins-agent-linux
iocage set allow_mount_procfs=1 jenkins-agent-linux
iocage set mount_fdescfs=1 jenkins-agent-linux
iocage set enforce_statfs=1 jenkins-agent-linux


cat <<EOT >> /etc/hosts
192.168.0.17 jenkins-agent-linux jenkins-agent-linux.fritz.box
EOT


# preparations needed on the host
pkg install debootstrap
sysrc linux_enable="YES"
sysrc linux_mounts_enable="NO"
service linux start


# back to jail: actual installation
debootstrap buster /usr/local/jails/iocage/jails/jenkins-agent-linux/root


iocage fstab -e jenkins-agent-linux
# add the following lines but without leading #
# devfs    /usr/local/jails/iocage/jails/jenkins-agent-linux/root/dev         devfs     rw          0 0
# tmpfs    /usr/local/jails/iocage/jails/jenkins-agent-linux/root/dev/shm     tmpfs     rw,size=1g,mode=1777 0 0
# fdescfs  /usr/local/jails/iocage/jails/jenkins-agent-linux/root/dev/fd      fdescfs   rw,linrdlnk 0 0
# linproc  /usr/local/jails/iocage/jails/jenkins-agent-linux/root/proc        linprocfs rw          0 0
# linsys   /usr/local/jails/iocage/jails/jenkins-agent-linux/root/sys         linsysfs  rw          0 0


iocage start jenkins-agent-linux


# install more needed software
iocage console jenkins-agent-linux

# e.g. use as a jenkins agent
apt update && apt install openjdk-11-jdk-headless openssh-server
systemctl enable ssh
service ssh start
adduser ci  # set and remind password


vim.tiny /etc/ssh/sshd_config
# StrictModes no
# PasswordAuthentication yes

# install more software to build projects
# apt install ninja cmake   # ...

exit
