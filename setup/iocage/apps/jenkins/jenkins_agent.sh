# This is highly dependent on your needs.
# So basic install applies for all situations, but the rest is up to you.

# not a thick jail, for bigger updates create new jail
iocage create -n jenkins-agent ip4_addr="lo1|192.168.0.4" -r 13.1-RELEASE
iocage set quota=20G jenkins-agent
iocage set boot=off jenkins-agent
# iocage set vmemoryuse="deny=4G" jenkins-agent  # this rule let git checkout fail
# iocage set cpuset=2,3 jenkins-agent


cat <<EOT >> /etc/hosts
192.168.0.4 jenkins-agent jenkins-agent.fritz.box
EOT


# no pf settings as this is an internal service


# only if poudriere active:
mkdir -p /usr/local/jails/iocage/jails/jenkins-agent/root/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/iocage/jails/jenkins-agent/root/usr/local/etc/pkg/repos/poudriere.conf
poudriere131: {
    url             : "http://192.168.0.13:8080/packages/131amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

cat <<EOT >> /usr/local/jails/iocage/jails/jenkins-agent/root/usr/local/etc/pkg/repos/freebsd.conf
FreeBSD: {
    enabled: no
}
EOT


### app setup
iocage start jenkins-agent
iocage pkg jenkins-agent update

# and whatever else is needed, e.g. boost-libs cmake ninja cxxtest cppcheck doxygen lcov
iocage pkg jenkins-agent install python38 python39 python310 libxml2 libxslt postgresql13-client py38-sqlite3 py39-sqlite3 py310-sqlite3 collectd5

# collectd
# set FQDNLookup=false, set hostname
# enable: df, disk, processes, swap, uptime
vim /usr/local/jails/iocage/jails/jenkins-agent/root/usr/local/etc/collectd.conf
iocage exec jenkins-agent service collectd enable
iocage exec jenkins-agent service collectd start


# limit JVM heap size globally
cat <<EOT >> /usr/local/jails/iocage/jails/jenkins-agent/root/usr/local/etc/javavm_opts.conf
JAVAVM_OPTS="-Xmx1G"
EOT


# assume we go for ssh agents
iocage pkg jenkins-agent install openjdk8 git collectd5
iocage exec jenkins-agent service sshd enable
iocage exec jenkins-agent service sshd start


# create user ci/ci
iocage console jenkins-agent
adduser ci  # set and remind password
exit

# add agent in Jenkins www frontend

# (1) UPGRADE in-place
iocage pkg jenkins-agent upgrade
iocage upgrade -r 13.1 jenkins-agent
