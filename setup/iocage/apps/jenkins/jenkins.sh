
iocage create -T -n jenkins ip4_addr="lo1|192.168.0.3" -r 13.1-RELEASE
iocage set quota=10G jenkins
iocage set boot=1 jenkins
iocage set allow_mount_procfs=1 jenkins
iocage set allow_mount_devfs=1 jenkins
iocage set mount_procfs=1 jenkins
iocage set mount_devfs=1 jenkins
iocage set depends="jenkins-agent jenkins-agent-linux" jenkins
# iocage set vmemoryuse="deny=4G" jenkins  # this rule let git checkout fail
# iocage set cpuset=2 jenkins

echo "rdr on $ext_if proto tcp from any to any port 8081 -> 192.168.0.3 port 8080" >> /etc/pf.conf
service pf restart


cat <<EOT >> /etc/hosts
192.168.0.3 jenkins jenkins.fritz.box
EOT


# only if poudriere active, then do modify repos:
mkdir -p /usr/local/jails/iocage/jails/jenkins/root/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/iocage/jails/jenkins/root/usr/local/etc/pkg/repos/poudriere.conf
poudriere131: {
    url             : "http://192.168.0.13:8080/packages/131amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

cat <<EOT >> /usr/local/jails/iocage/jails/jenkins/root/usr/local/etc/pkg/repos/freebsd.conf
FreeBSD: {
    enabled: no
}
EOT


# jenkins <2.356 (non-LTS) does NOT work for me
# jenkins-lts <2.346.1 plugins are partially unmaintained and not installable

iocage start jenkins
iocage pkg jenkins update
iocage pkg jenkins install jenkins-lts vim-tiny git-tiny collectd5


vim /usr/local/jails/iocage/jails/jenkins/root/usr/local/etc/rc.d/jenkins
# add after line 56: (if xmx is too small, strange issues like "resource temporarily unavailable" occur)
# ": ${jenkins_java_opts="-Xmx4G"}"
# modify optionally
# "": ${jenkins_java_home="/usr/local/openjdk11"}


iocage exec jenkins service jenkins enable
iocage exec jenkins service jenkins start

# wait until Jenkins http://server.fritz.box:8081/ is available and asks for initial admin password
# then get token with following command and use it
iocage exec jenkins cat /usr/local/jenkins/secrets/initialAdminPassword


# collectd
# set FQDNLookup=false, set hostname="jenkins"
# enable: df, disk, processes, swap, uptime
vim /usr/local/jails/iocage/jails/jenkins/root/usr/local/etc/collectd.conf

iocage exec jenkins service collectd enable
iocage exec jenkins service collectd start

# Initial Jenkins plugins (just for me):
# - SSH Build Agents
# - git
# - Pipeline
# - Pyenv Pipeline (python env)
# - JUnit + Cobertura + Warnings Next Generation
# - (don't go for Blue Ocean until you have "enough" memory)

# Jenkins Setup
# - overall
#   - Disable build processors on master and go for exclusive projects only
#   - Add agent "agent-bsd", 2 executers, /home/ci, labels "bsd freebsd agent", start over ssh 192.168.0.4 with user ci, no verification, always on (on-demand start does not work)
#   - Add agent "agent-linux", 2 executers, /home/ci, labels "linux debian agent", start over ssh 192.168.0.17 with user ci, no verification, stop after 10m, extended options: -Xmx256m


# (1) UPGRADE in-place
iocage pkg jenkins update
iocage pkg jenkins upgrade
iocage upgrade -r 13.1 jenkins
