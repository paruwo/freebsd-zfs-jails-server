
# http://server.fritz.box:8087

iocage create -T -n airflow ip4_addr="lo1|192.168.0.7" -r 13.1-RELEASE
iocage set quota=10G airflow
iocage set depends="postgresql14 redis" airflow
iocage set vmemoryuse="deny=2G" airflow
# iocage set cpuset=6,7 airflow


echo 'rdr on $ext_if proto tcp from any to any port 8087 -> 192.168.0.7 port 8080' >> /etc/pf.conf
echo 'rdr on $ext_if proto tcp from any to any port 5555 -> 192.168.0.7 port 5555' >> /etc/pf.conf
service pf restart


cat <<EOT >> /etc/hosts
192.168.0.7 airflow airflow.fritz.box
EOT


## only if poudriere active, then do modify repos:
mkdir -p /usr/local/jails/iocage/jails/airflow/root/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/iocage/jails/airflow/root/usr/local/etc/pkg/repos/poudriere.conf
poudriere131: {
    url             : "http://192.168.0.13:8080/packages/131amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT


iocage start airflow
iocage pkg airflow update
iocage pkg airflow install python38 git-tiny tmux vim-tiny bash postgresql14-contrib \
                           py38-sqlite3 py38-virtualenv py38-cryptography

# setup app user
iocage exec airflow pw user add airflow -m


# install airflow via python
iocage console airflow
su - airflow
python3.8 -m ensurepip --user
python3.8 -m pip install --upgrade pip setuptools --user
python3.8 -m pip install psycopg2-binary --user

export SLUGIFY_USES_TEXT_UNIDECODE=yes
python3.8 -m pip install apache-airflow==2.3.2 --user
python3.8 -m pip install apache-airflow[crypto,redis,celery,async,postgresql]==2.3.2 --user


# Assumption is that jails redis + postgresql are up & running
# this is $AIRFLOW_HOME
mkdir -p /home/airflow/airflow
cat <<EOT >> /home/airflow/airflow/airflow.cfg
[core]
default_timezone = Europe/Berlin
sql_alchemy_conn = postgresql+psycopg2://airflow:airflow@192.168.0.214:5432/airflow
executor = CeleryExecutor
load_examples = False

[webserver]
base_url = http://localhost:8080
web_server_host = 192.168.0.7
default_ui_timezone = Europe/Berlin

[scheduler]
dag_dir_list_interval = 30

[celery]
broker_url = redis://192.168.0.6:6379/0
result_backend = db+postgres://airflow:airflow@192.168.0.2:5432/airflow
EOT


# create initial user
iocage exec --jail_user airflow airflow \
  airflow users create --role Admin --username admin --email admin --firstname admin --lastname admin --password admin


# dependencies, add postgresql database user
iocage console postgresql14
su - postgres
psql
CREATE USER airflow WITH PASSWORD 'airflow' CREATEDB;
CREATE DATABASE airflow OWNER airflow;
\q
exit


# one-time initialization
~/.local/bin/airflow db init
echo "export PATH=$PATH:~/.local/bin" >> ~/.profile

# exit airflow user & jail, back on "host"
exit
exit



# in doubt check jail with
# iocage console airflow
# su - airflow
# nohup /usr/home/airflow/startup.sh &

# configure start/stop scripts
# @host
cat <<EOF >> /usr/local/jails/iocage/jails/airflow/root/home/airflow/startup.sh
#!/bin/sh
nohup ~/.local/bin/airflow celery worker --daemon &
nohup ~/.local/bin/airflow scheduler --daemon &
nohup ~/.local/bin/airflow webserver --daemon &
EOF

cat <<EOF >> /usr/local/jails/iocage/jails/airflow/root/home/airflow/stop.sh
#!/bin/sh
kill $(ps aux | grep 'airflow webserver' | awk '{print $2}')
EOF

iocage exec airflow chown airflow:airflow /home/airflow/startup.sh
iocage exec airflow chown airflow:airflow /home/airflow/stop.sh
iocage exec airflow chmod +x /home/airflow/startup.sh
iocage exec airflow chmod +x /home/airflow/stop.sh


# startup
# jexec -l -U airflow airflow /home/airflow/startup.sh
iocage exec --jail_user airflow airflow sh /home/airflow/startup.sh



# error handling

# debug
# go through all commands in startup.sh separately and call them as airflow user
# there should be no error
iocage console airflow
su - airflow
~/.local/bin/airflow celery worker
# ...


# clean up any mess by recreate the database
iocage console airflow
su - airflow
~/.local/bin/airflow db reset
~/.local/bin/airflow users create --role Admin --username admin --email admin --firstname admin --lastname admin --password admin
