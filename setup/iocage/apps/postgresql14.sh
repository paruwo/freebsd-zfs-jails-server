
# Initial setup only, see below for major database upgrades

iocage create -T -n postgresql14 ip4_addr="lo1|192.168.0.214" -r 13.1-RELEASE
zfs create -o canmount=off -o compression=lz4 -o atime=off -o quota=50G jails/iocage/jails/postgresql14/root/data
iocage set boot=1 postgresql14
iocage set quota=55G postgresql14
iocage set sysvmsg=new postgresql14
iocage set sysvsem=new postgresql14
iocage set sysvshm=new postgresql14
iocage set vmemoryuse="deny=2G" postgresql14
# iocage set cpuset=0,1 postgresql14

cat "rdr on $ext_if proto tcp from any to any port 5434 -> 192.168.0.214 port 5432" >> /etc/pf.conf
service pf restart


cat <<EOT >> /etc/hosts
192.168.0.214 postgresql14 postgresql14.fritz.box
EOT


# only if poudriere active, then do modify repos:
mkdir -p /usr/local/jails/iocage/jails/postgresql14/root/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/iocage/jails/postgresql14/root/usr/local/etc/pkg/repos/poudriere.conf
poudriere131: {
    url             : "http://192.168.0.13:8080/packages/131amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

# .. and disable official repo if you are going to use it
cat <<EOT >> /usr/local/jails/iocage/jails/postgresql14/root/usr/local/etc/pkg/repos/freebsd.conf
FreeBSD: {
    enabled: no
}
EOT


iocage start postgresql14
iocage pkg postgresql14 update
iocage pkg postgresql14 install postgresql14-server postgresql14-contrib vim-tiny
iocage exec postgresql14 service postgresql enable

# COLLECTD NOT WORKING WITH POSTGRES14
# set FQDNLookup=false, set hostname, activate and configure postgres plugin
# vim /usr/local/jails/iocage/jails/postgresql14/root/usr/local/etc/collectd.conf
# configure network: 192.168.0.10
# configure postgresql: databases weather + postgres + ...
# configure cpu: ReportByState true, ReportByCpu true, ValuesPercentage	true, ReportNumCpu



# init db initially
iocage exec postgresql14 /usr/local/etc/rc.d/postgresql oneinitdb

# rename data and wal locations to moved to separate datasets
mv /usr/local/jails/iocage/jails/postgresql14/root/var/db/postgres/data14/pg_wal \
   /usr/local/jails/iocage/jails/postgresql14/root/var/db/postgres/data14/pg_wal-old
mv /usr/local/jails/iocage/jails/postgresql14/root/var/db/postgres/data14/base \
   /usr/local/jails/iocage/jails/postgresql14/root/var/db/postgres/data14/base-old


# create new datasets for data and wal
zfs create -o recordsize=8k -o redundant_metadata=most -o primarycache=metadata \
    -o mountpoint=/usr/local/jails/iocage/jails/postgresql14/root/var/db/postgres/data14/pg_wal \
    -o logbias=throughput jails/iocage/jails/postgresql14/root/data/pg_wal
zfs create -o recordsize=8k -o redundant_metadata=most -o primarycache=metadata \
    -o mountpoint=/usr/local/jails/iocage/jails/postgresql14/root/var/db/postgres/data14/base \
    -o logbias=throughput jails/iocage/jails/postgresql14/root/data/base

# copy back stuff
cp -Rp /usr/local/jails/iocage/jails/postgresql14/root/var/db/postgres/data14/pg_wal-old/* \
       /usr/local/jails/iocage/jails/postgresql14/root/var/db/postgres/data14/pg_wal
cp -Rp /usr/local/jails/iocage/jails/postgresql14/root/var/db/postgres/data14/base-old/* \
       /usr/local/jails/iocage/jails/postgresql14/root/var/db/postgres/data14/base
rm -rf /usr/local/jails/iocage/jails/postgresql14/root/var/db/postgres/data14/pg_wal-old
rm -rf /usr/local/jails/iocage/jails/postgresql14/root/var/db/postgres/data14/base-old



iocage exec postgresql14 chown -R postgres:postgres /var/db/postgres/data14/
iocage exec postgresql14 service postgresql onestart

# setup connectivity
iocage exec postgresql14
su - postgres
psql
> ALTER USER postgres WITH ENCRYPTED PASSWORD '...';
> \q
echo 'host all all 192.168.0.0/16 md5' >> /var/db/postgres/data14/pg_hba.conf

vim data14/postgresql.conf
# switch parameters:
# max_connections = 30
# full_page_writes=off
# wal_level = logical

service postgresql restart
exit
exit  # to host



# SSL
iocage exec postgresql14

cd /var/db/postgres/data14/
openssl req -new -x509 -nodes -text -out server.crt -keyout server.key -subj "/CN=server.fritz.box"
chmod og-rwx server.key

# set "ssl=on"
vim postgresql.conf

chown postgres:postgres server.*
service postgresql restart
exit





# optional: benchmark
# we benchmark directly on server because client is installed, this is not 100% serious
iocage console postgresql14
su - postgres

# small set
pgbench -i postgres

# select only, 2 clients, 20 seconds on database weather: 60k TPS
pgbench -S -c 2 -M prepared -T 20 postgres
# mixed read+write: 100 TPS (sic, not K or M)
pgbench -c 2 -M prepared -T 20 postgres
# bigger data set
# pgbench -i -s 100 postgres

# cleanup
psql postgres -c "drop table pgbench_accounts"
psql postgres -c "drop table pgbench_branches"
psql postgres -c "drop table pgbench_history"
psql postgres -c "drop table pgbench_tellers"




##### IT'S POSTGRESQL MAJOR UPGRADE TIME
##### ONLY EXECUTE IF YOU UPGRADE FROM OLD TO THIS JAIL

# For major Postgresql upgrade we go the replication way
# see https://www.2ndquadrant.com/en/blog/upgrading-to-postgresql-11-with-logical-replication/


# old server
vim /usr/local/jails/iocage/jails/postgresql/root/var/db/postgres/data13/postgresql.conf
# ensure that parameter is set: wal_level = logical
# if changed, then restart old server with "iocage exec postgresql service postgresql restart"

# verify that connection from new jail to old server works
iocage exec postgresql14 psql -U postgres -h 192.168.0.2 -c 'select version();'

# copy schema (this will ask for password once per database)
iocage exec postgresql14 pg_dumpall -s -U postgres -h 192.168.0.2 -s >schemadump.sql
# ...then import to new server
iocage exec postgresql14 psql -d postgres -U postgres -f schemadump.sql

# now it's about the data - after having created the subscription(s) the data starts to flow
# per database: create a publication ...
iocage exec postgresql14 psql -U postgres -h 192.168.0.2 -d weather -c 'CREATE PUBLICATION p_upgrade FOR ALL TABLES;'

# ...and finally the subscription (not in one command to work around ticks issues)
iocage exec postgresql14
psql -U postgres -d weather
> CREATE SUBSCRIPTION s_upgrade CONNECTION 'host=192.168.0.2 port=5432 dbname=weather user=postgres password=<pass>' PUBLICATION p_upgrade;
> \q
# optionally track status
> select * from pg_subscription_rel;
> select * from pg_stat_subscription;


# redirect all apps to new database postgresql14


# all good? great, then stop old server and cleanup
iocage exec postgresql14 psql -U postgres -d weather -c 'DROP SUBSCRIPTION s_upgrade;'
iocage stop postgresql
iocage set boot=no postgresql
iocage set depends="postgresql14 influxdb" grafana
iocage set depends="postgresql14 redis" airflow
# finally purge the old jail
# iocage destroy --recursive postgresql
# iocage exec postgresql14 rm /root/schemadump.sql



# (1) UPGRADE in-place
iocage pkg postgresql14 update
iocage pkg postgresql14 upgrade
iocage upgrade -r 13.1 postgresql14
