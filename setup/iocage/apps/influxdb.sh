
iocage create -T -n influxdb ip4_addr="lo1|192.168.0.10" -r 13.1-RELEASE
iocage set quota=50G influxdb
iocage set boot=1 influxdb
iocage set vmemoryuse="deny=2G" influxdb
# iocage set cpuset=6,7 influxdb


# only if poudriere active, then do modify repos:
mkdir -p /usr/local/jails/iocage/jails/influxdb/root/usr/local/etc/pkg/repos/
cat <<EOT >> /usr/local/jails/iocage/jails/influxdb/root/usr/local/etc/pkg/repos/poudriere.conf
poudriere131: {
    url             : "http://192.168.0.13:8080/packages/131amd64-default/",
    enabled         : yes,
    mirror_type     : "http",
    priority        : 10
}
EOT

cat <<EOT >> /usr/local/jails/iocage/jails/influxdb/root/usr/local/etc/pkg/repos/freebsd.conf
FreeBSD: {
    enabled: no
}
EOT


cat <<EOT >> /etc/hosts
192.168.0.10 influxdb influxdb.fritz.box
EOT


cat <<EOT >> /etc/pf.conf
### influxdb
## outgoing
rdr on $ext_if proto tcp from any to any port 8082 -> 192.168.0.10 port 3000
rdr on $ext_if proto tcp from any to any port 8086 -> 192.168.0.10 port 8086
## incoming
# collectd
rdr on $ext_if proto udp from any to any port 25826 -> 192.168.0.10 port 25826
# udp
rdr on $ext_if proto tcp from any to any port 8089 -> 192.168.0.10 port 8089
# weather
rdr on $ext_if proto tcp from any to any port 8090 -> 192.168.0.10 port 8090
EOT

service pf restart


iocage start influxdb
iocage pkg influxdb install influxdb vim-tiny collectd5

iocage exec influxdb service influxd enable
iocage exec influxdb service influxd start
iocage exec influxdb sysrc influxd_user="root"
iocage exec influxdb sysrc influxd_group="wheel"



# set hostname, set FQDNLookup to false
vim /usr/local/jails/iocage/jails/influxdb/root/usr/local/etc/collectd.conf
# configure network: 192.168.0.10

# start only once to get types.db
iocage exec influxdb service collectd onestart
iocage exec influxdb service collectd onestop


vim /usr/local/jails/iocage/jails/influxdb/root/usr/local/etc/influxdb.conf
# [[collectdb]]
# enable = true
# typesdb = "/usr/local/share/collectd/types.db"



# generic setup and config
iocage exec influxdb
influx -unsafeSsl
create user admin with password 'admin' with all privileges
create database weather
create database collectd with duration 104w
exit  # from influx shell



# ssl
iocage exec influxdb
openssl req -x509 -nodes -newkey rsa:2048 -keyout /etc/ssl/influxdb-selfsigned.key -out /etc/ssl/influxdb-selfsigned.crt -days 10000
chown influxd:influxd /etc/ssl/influxdb-selfsigned.*

vim /usr/local/etc/influxd.conf
# check [http] section
https-enabled=true
https-certificate = "/etc/ssl/influxdb-selfsigned.crt"
https-private-key = "/etc/ssl/influxdb-selfsigned.key"

service influxd restart

# test connectivity from inside jail
influx -ssl -unsafeSsl
# test connectivity from server
influx -ssl -unsafeSsl -host 192.168.0.10



# (optionally) enable internal monitoring and reporting
# in section [monitor] set store_enabled=true



# (1) UPGRADE in-place
iocage pkg influxdb update
iocage pkg influxdb upgrade
iocage upgrade -r 13.1 influxdb

# (2) UPGRADE moving the data to new jail
iocage set vmemoryuse="deny=4G" influxdb
iocage console influxdb
influxdb influx_inspect export -compress -datadir /var/db/influxdb/data/ -waldir /var/db/influxdb/wal/ -out ./influx.dmp
exit
cp /usr/local/jails/iocage/jails/influxdb/root/influxdb.dmp .

# setup with new version

cp influxdb.dmp /usr/local/jails/iocage/jails/influxdb/root/
iocage console influxdb
iocage exec influxdb influx -import -path=influx.dmp -compressed
rm influxdb.dmp
exit

rm /usr/local/jails/iocage/jails/influxdb/root/influxdb.dmp
