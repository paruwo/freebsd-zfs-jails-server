
cat <<EOT >> /boot/load.conf
# load bhyve virtualization module
vmm_load="YES"
EOT


# define ZFS location for bhyve VM's
zfs create -o mountpoint=/usr/local/jails/bhyve jails/bhyve

# install helper package "vm" and configure (additionally does extend /etc./rc.conf)
pkg install vm-bhyve
sysrc vm_enable="YES"
sysrc vm_dir="zfs:jails/bhyve"
vm init

# create a virtual network interface and bridge to lan adapter
vm switch create public
vm switch add public re0

# if samples are missing due to ports config, when find then there:
# https://github.com/churchers/vm-bhyve/blob/master/sample-templates/alpine.conf
fetch https://raw.githubusercontent.com/churchers/vm-bhyve/master/sample-templates/default.conf /usr/local/jails/bhyve/.templates/default.conf
