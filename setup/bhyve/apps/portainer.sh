
# to start it: vm start portainer (app starts automatically)
# http://portainer.fritz.box:9000
# admin/...


# prepare Debian Linux, if not done yet
vm iso https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.3.0-amd64-netinst.iso
fetch https://raw.githubusercontent.com/churchers/vm-bhyve/master/sample-templates/debian.conf /usr/local/jails/bhyve/.templates/debian.conf

# create Linux VM
vm create -t debian portainer
# potentially increase #CPUs, RAM and disk size
vm configure portainer
vm install portainer debian-11.3.0-amd64-netinst.iso


# enter VM and go through non-graphical installation
#  - enter remarkable hostname like "portainer.fritz.box"
#  - at the end also install grub (then gets restarted)
vm console debian


# ... then install docker
# following https://www.atlantic.net/dedicated-server-hosting/how-to-install-portainer-docker-ui-web-interface-on-debian-11/
apt-get install apt-transport-https ca-certificates curl gnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

apt-get update -y
apt-get install docker-ce docker-ce-cli containerd.io -y

# check
docker version


# install docker compose
wget https://github.com/docker/compose/releases/download/v2.2.2/docker-compose-linux-x86_64
cp docker-compose-linux-x86_64 /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# check
docker-compose --version


# install portainer and run
docker volume create portainer_data
docker pull portainer/portainer-ce:latest
docker run -d -p 8000:8000 -p 9000:9000 --name=portainer --restart=always \
           -v /var/run/docker.sock:/var/run/docker.sock \
           -v portainer_data:/data portainer/portainer-ce:latest


# check
docker ps

# now open
# http://portainer.fritz.box:9000
