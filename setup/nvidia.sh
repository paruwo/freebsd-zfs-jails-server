# This is not necessarily needed, only on demand.
# After having tried out a couple of tutorials, only this worked.

# assuming that you want a graphical user interface as well as efficient XFCE
pkg -4 install xorg xcfe
pw groupmod video -m ceberhar

# for xfce only
sysrc dbus_enable="YES"
sysrc hald_enable="YES"
service dbus enable
service hald enable

# if this does not work, move this entry to the top of loader.conf
echo 'kern.vty=vt' >> /boot/loader.conf

mkdir nvidia && cd nvidia
curl -O https://de.download.nvidia.com/XFree86/FreeBSD-x86_64/470.86/NVIDIA-FreeBSD-x86_64-470.86.tar.xz
tar -xf NVIDIA-FreeBSD-x86_64-470.86.tar.xz

cd NVIDIA-FreeBSD-x86_64-470.86
make install

nvidia-xconfig
touch /usr/local/etc/X11/xorg.conf.d/driver-nvidia.conf

echo ". /usr/local/etc/xdg/xfce4/xinitrc" > ~/.xinitrc

startx
