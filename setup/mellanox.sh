# This is not necessarily needed, only on demand.

# for later modity boot.loader.conf
echo 'mlx4_load="YES"' >> boot.loader.conf
echo 'ibcore_load="YES"' >> boot.loader.conf
echo 'mlx4ib_load="YES"' >> boot.loader.conf
echo 'ipoib_load="YES"' >> boot.loader.conf
echo 'mlx4en_load="YES"' >> boot.loader.conf

# turn on once for this session only
kldload mlx mlx4en
ifconfig mlxen0 inet 10.0.1.2 netmask 255.255.255.0

# test performance
pkg -4 install iperf

# switch to NAS and run ...
iperf -s
# ... on server run
iperf -c 10.0.1.1 # should get >=7.4 GB/s
